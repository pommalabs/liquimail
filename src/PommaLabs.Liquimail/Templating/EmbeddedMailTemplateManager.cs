﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.IO;
using System.Reflection;
using DotLiquid;
using DotLiquid.Exceptions;
using DotLiquid.FileSystems;
using PommaLabs.Liquimail.Resources;

namespace PommaLabs.Liquimail.Templating;

/// <summary>
///   Loads templates from assembly embedded resources.
/// </summary>
public sealed class EmbeddedMailTemplateManager : AbstractMailTemplateManager
{
    private readonly Assembly _assembly;
    private readonly EmbeddedFileSystem _embeddedFileSystem;

    /// <summary>
    ///   <para>
    ///     Sets the <see cref="Template.FileSystem"/> property with a properly configured
    ///     <see cref="EmbeddedFileSystem"/> implementation.
    ///   </para>
    ///   <para>
    ///     Its behavior is the same as with the Local File System, except this uses namespaces
    ///     and embedded resources instead of directories and files.
    ///   </para>
    ///   <para>Example:</para>
    ///   <para>var fileSystem = new EmbeddedFileSystem("My.Base.Namespace");</para>
    ///   <para>fileSystem.FullPath("mypartial"); // "My.Base.Namespace._mypartial.liquid"</para>
    ///   <para>fileSystem.FullPath("dir/mypartial"); // "My.Base.Namespace.dir._mypartial.liquid"</para>
    /// </summary>
    /// <param name="assembly">The assembly in which all templates are stored.</param>
    /// <param name="rootNamespace">
    ///   The root namespace which will be used as prefix for all paths.
    /// </param>
    public EmbeddedMailTemplateManager(Assembly assembly, string rootNamespace)
    {
        // Preconditions
        if (assembly == null) throw new ArgumentNullException(nameof(assembly), ErrorMessages.MailTemplateManager_NullAssembly);
        if (string.IsNullOrWhiteSpace(rootNamespace)) throw new System.ArgumentException(ErrorMessages.MailTemplateManager_MissingRootNamespace, nameof(rootNamespace));

        var fs = new EmbeddedFileSystem(assembly, rootNamespace);
        Template.FileSystem = fs;
        _embeddedFileSystem = fs;
        _assembly = assembly;
    }

    /// <inheritdoc/>
    protected override string MasterFullPath(string templatePath)
    {
        var tmpFullPath = _embeddedFileSystem.FullPath(templatePath);
        var liquidTemplateFile = $"{Path.GetFileName(templatePath)}.liquid";
        var startIndex = tmpFullPath.IndexOf(liquidTemplateFile, StringComparison.Ordinal);
        return tmpFullPath.Remove(startIndex - 1, 1);
    }

    /// <inheritdoc/>
    protected override string ReadMasterTemplateFile(string fullPath)
    {
        var stream = _assembly.GetManifestResourceStream(fullPath);
        if (stream == null)
        {
            throw new FileSystemException(ErrorMessages.MailTemplateManager_TemplatePathNotFound, fullPath);
        }
        using var reader = new StreamReader(stream);
        return reader.ReadToEnd();
    }
}
