﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using DotLiquid;
using DotLiquid.Exceptions;

namespace PommaLabs.Liquimail.Templating;

/// <summary>
///   Mail template handling is completely based on the useful DotLiquid library. This interface
///   simply adds LRU cache modeling and a custom template retrieval.
/// </summary>
public interface IMailTemplateManager
{
    /// <summary>
    ///   Loads the template associated with given template name. The template is also cached
    ///   for later usage.
    /// </summary>
    /// <param name="templateName">The template name.</param>
    /// <returns>The required template.</returns>
    /// <exception cref="System.ArgumentException">Template name is null, empty or blank.</exception>
    /// <exception cref="FileSystemException">
    ///   No template with specified name could be loaded, or template name is not valid.
    /// </exception>
    Template LoadTemplate(string templateName);
}
