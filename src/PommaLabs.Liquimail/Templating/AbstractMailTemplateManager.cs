﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using DotLiquid;
using DotLiquid.Exceptions;
using PommaLabs.Liquimail.Core;
using PommaLabs.Liquimail.Resources;

namespace PommaLabs.Liquimail.Templating;

/// <summary>
///   Abstract implementation of a mail template manager.
/// </summary>
public abstract class AbstractMailTemplateManager : IMailTemplateManager
{
    /// <summary>
    ///   A concurrent LRU cache used to store loaded templates.
    /// </summary>
    private ConcurrentLRUCache<string, Template> _lruCache;

    /// <summary>
    ///   The maximum capacity of the LRU cache.
    /// </summary>
    private int _lruCacheCapacity = 16;

    /// <summary>
    ///   Configures the internal LRU cache.
    /// </summary>
    protected AbstractMailTemplateManager()
    {
        _lruCache = new ConcurrentLRUCache<string, Template>(_lruCacheCapacity);
    }

    /// <summary>
    ///   The maximum capacity of the LRU cache. Default value is 10.
    /// </summary>
    public int LRUCacheCapacity
    {
        get { return _lruCacheCapacity; }
        set
        {
            // LRU cache constructor validates the capacity value.
            _lruCache = new ConcurrentLRUCache<string, Template>(value);
            _lruCacheCapacity = value;
        }
    }

    /// <inheritdoc/>
    public Template LoadTemplate(string templateName)
    {
        // Preconditions
        if (string.IsNullOrWhiteSpace(templateName)) throw new System.ArgumentException(ErrorMessages.MailSender_MissingTemplateName, nameof(templateName));

        if (_lruCache.TryGetValue(templateName, out var maybeTemplate))
        {
            return maybeTemplate;
        }

        var fullPath = MasterFullPath(templateName);
        var source = ReadMasterTemplateFile(fullPath);
        var template = Template.Parse(source);
        _lruCache.Add(templateName, template);
        return template;
    }

    /// <summary>
    ///   Gets the full path of the master template.
    /// </summary>
    /// <param name="templatePath">The relative template path.</param>
    /// <returns>The full path of the master template.</returns>
    /// <exception cref="FileSystemException">Template path is not valid.</exception>
    protected abstract string MasterFullPath(string templatePath);

    /// <summary>
    ///   Reads the contents of the master template.
    /// </summary>
    /// <param name="fullPath">The full template path.</param>
    /// <returns>The contents of the master template.</returns>
    /// <exception cref="FileSystemException">
    ///   No template with specified path could be loaded.
    /// </exception>
    protected abstract string ReadMasterTemplateFile(string fullPath);
}
