﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.IO;
using DotLiquid;
using DotLiquid.Exceptions;
using DotLiquid.FileSystems;
using PommaLabs.Liquimail.Resources;

namespace PommaLabs.Liquimail.Templating;

/// <summary>
///   Loads templates from the file system.
/// </summary>
public sealed class LocalMailTemplateManager : AbstractMailTemplateManager
{
    private readonly LocalFileSystem _localFileSystem;

    /// <summary>
    ///   <para>
    ///     Sets the <see cref="Template.FileSystem"/> property with a properly configured
    ///     <see cref="LocalFileSystem"/> implementation.
    ///   </para>
    ///   <para>
    ///     For security reasons, template paths are only allowed to contain letters, numbers,
    ///     and underscore.
    ///   </para>
    ///   <para>Example:</para>
    ///   <para>var fileSystem = new LocalFileSystem.new("/some/path");</para>
    ///   <para>fileSystem.FullPath("mypartial"); // "/some/path/_mypartial.liquid"</para>
    ///   <para>fileSystem.FullPath("dir/mypartial"); // "/some/path/dir/_mypartial.liquid"</para>
    /// </summary>
    /// <param name="absolutePath">The path in which all templates are stored.</param>
    public LocalMailTemplateManager(string absolutePath)
    {
        // Preconditions
        if (string.IsNullOrWhiteSpace(absolutePath)) throw new ArgumentException(ErrorMessages.MailTemplateManager_MissingAbsolutePath, nameof(absolutePath));

        var fs = new LocalFileSystem(absolutePath);
        Template.FileSystem = fs;
        _localFileSystem = fs;
    }

    /// <inheritdoc/>
    protected override string MasterFullPath(string templatePath)
    {
        var tmpFullPath = _localFileSystem.FullPath(templatePath);
        var directoryName = Path.GetDirectoryName(tmpFullPath);
        var fileName = Path.GetFileName(tmpFullPath).Substring(1);
        return Path.Combine(directoryName, fileName);
    }

    /// <inheritdoc/>
    protected override string ReadMasterTemplateFile(string fullPath)
    {
        if (!File.Exists(fullPath))
        {
            throw new FileSystemException(ErrorMessages.MailTemplateManager_TemplatePathNotFound, fullPath);
        }
        return File.ReadAllText(fullPath);
    }
}
