﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using DotLiquid;
using PommaLabs.Liquimail.Resources;

namespace PommaLabs.Liquimail.Templating;

/// <summary>
///   Throws an instance of <see cref="NotSupportedException"/> when called. This class is
///   useful when current <see cref="IMailSender"/> does not support mail templates management.
/// </summary>
public sealed class NotSupportedMailTemplateManager : IMailTemplateManager
{
    /// <summary>
    ///   Static instance, used as default template manager when none has been specified.
    /// </summary>
    public static NotSupportedMailTemplateManager Instance { get; } = new();

    /// <inheritdoc/>
    public Template LoadTemplate(string templateName)
    {
        throw new NotSupportedException(ErrorMessages.MailTemplateManager_NotSupported);
    }
}
