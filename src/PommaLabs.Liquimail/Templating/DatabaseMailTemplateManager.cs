﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Text.RegularExpressions;
using DotLiquid;
using DotLiquid.Exceptions;
using DotLiquid.FileSystems;
using PommaLabs.Liquimail.Core;
using PommaLabs.Liquimail.Resources;

namespace PommaLabs.Liquimail.Templating;

/// <summary>
///   <para>
///     This implements a file system which retrieves template files from a SQL database table.
///   </para>
///   <para>
///     Its behavior is the same as with the Local File System, except this uses a SQL database
///     table instead of directories and files.
///   </para>
///   <para>Example:</para>
///   <para>var fileSystem = new DatabaseFileSystem(...);</para>
///   <para>fileSystem.FullPath("mypartial"); // "_mypartial"</para>
///   <para>fileSystem.FullPath("dir/mypartial"); // "dir/_mypartial"</para>
/// </summary>
public sealed class DatabaseFileSystem : IFileSystem
{
    private readonly string _connectionString;
    private readonly DbProviderFactory _dbProviderFactory;
    private readonly string _parameterPrefix;
    private readonly string _pathColumnName;
    private readonly string _schemaName;
    private readonly string _tableName;
    private readonly string _templateColumnName;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="dbProviderFactory">Database provider factory.</param>
    /// <param name="connectionString">Connection string.</param>
    /// <param name="schemaName">
    ///   Name of the SQL schema which contains the mail templates table.
    /// </param>
    /// <param name="tableName">Mail templates table name.</param>
    /// <param name="parameterPrefix">SQL command parameter prefix.</param>
    /// <param name="pathColumnName">Path column name.</param>
    /// <param name="templateColumnName">Template column name.</param>
    public DatabaseFileSystem(
        DbProviderFactory dbProviderFactory,
        string connectionString,
        string schemaName = Defaults.SchemaName,
        string tableName = Defaults.TableName,
        string parameterPrefix = Defaults.ParameterPrefix,
        string pathColumnName = Defaults.PathColumnName,
        string templateColumnName = Defaults.TemplateColumnName)
    {
        _dbProviderFactory = dbProviderFactory ?? throw new ArgumentNullException(nameof(dbProviderFactory), ErrorMessages.MailTemplateManager_NullDbProviderFactory);
        _connectionString = string.IsNullOrWhiteSpace(connectionString) ? throw new System.ArgumentException(ErrorMessages.MailTemplateManager_MissingConnectionString, nameof(connectionString)) : connectionString;
        _schemaName = schemaName ?? Defaults.SchemaName;
        _tableName = tableName ?? Defaults.TableName;
        _parameterPrefix = parameterPrefix ?? Defaults.ParameterPrefix;
        _pathColumnName = pathColumnName ?? Defaults.PathColumnName;
        _templateColumnName = templateColumnName ?? Defaults.TemplateColumnName;
    }

    /// <inheritdoc/>
    public string ReadTemplateFile(Context context, string templateName)
    {
        ArgumentNullException.ThrowIfNull(context);

        var templatePath = (string)context[templateName];
        var fullPath = FullPath(templatePath, false);
        return ReadTemplateFileInternal(fullPath);
    }

    /// <summary>
    ///   Returns the full path from given relative template path.
    /// </summary>
    /// <param name="templatePath">Template path.</param>
    /// <param name="isMaster">Whether given template is master or not.</param>
    /// <returns>The full path from given relative template path.</returns>
    internal static string FullPath(string templatePath, bool isMaster)
    {
        if (templatePath == null || !Regex.IsMatch(templatePath, @"^[^.\/][a-zA-Z0-9_\/]+$", default, Constants.RegexMatchTimeout))
        {
            throw new FileSystemException(ErrorMessages.MailTemplateManager_IllegalTemplateName, templatePath);
        }

        var basePath = templatePath.Contains('/')
            ? Path.GetDirectoryName(templatePath)
            : string.Empty;

        var fileName = isMaster
            ? Path.GetFileName(templatePath)
            : $"_{Path.GetFileName(templatePath)}";

        return Path.Combine(basePath, fileName).Replace('\\', '/');
    }

    /// <summary>
    ///   Reads the contents of the specified template.
    /// </summary>
    /// <param name="fullPath">The full template path.</param>
    /// <returns>The contents of the specified template.</returns>
    internal string ReadTemplateFileInternal(string fullPath)
    {
        var schemaNameWithDot = string.IsNullOrWhiteSpace(_schemaName)
            ? string.Empty
            : $"{_schemaName}.";

        using var dbConnection = _dbProviderFactory.CreateConnection();

        dbConnection.ConnectionString = _connectionString;
        dbConnection.Open();

        using var command = dbConnection.CreateCommand();

        // Query below is composed using library configurations, not user inputs.
        command.CommandType = CommandType.Text;
        command.CommandText = $@"
            select {_templateColumnName}
            from {schemaNameWithDot}{_tableName}
            where {_pathColumnName} = {_parameterPrefix}path
        ";

        var pathParameter = command.CreateParameter();

        pathParameter.ParameterName = "path";
        pathParameter.Value = fullPath;
        command.Parameters.Add(pathParameter);

        using var reader = command.ExecuteReader();

        if (!reader.Read())
        {
            throw new FileSystemException(ErrorMessages.MailTemplateManager_TemplatePathNotFound, fullPath);
        }
        return reader.GetString(0);
    }

    /// <summary>
    ///   Default values for constructor arguments.
    /// </summary>
    internal static class Defaults
    {
        /// <summary>
        ///   Default parameter prefix.
        /// </summary>
        public const string ParameterPrefix = "@";

        /// <summary>
        ///   Default path column name.
        /// </summary>
        public const string PathColumnName = "Path";

        /// <summary>
        ///   Default schema name.
        /// </summary>
        public const string SchemaName = "";

        /// <summary>
        ///   Default table name.
        /// </summary>
        public const string TableName = "MailTemplates";

        /// <summary>
        ///   Default template column name.
        /// </summary>
        public const string TemplateColumnName = "Template";
    }
}

/// <summary>
///   Loads templates from a SQL database table.
/// </summary>
public sealed class DatabaseMailTemplateManager : AbstractMailTemplateManager
{
    private readonly DatabaseFileSystem _databaseFileSystem;

    /// <summary>
    ///   <para>
    ///     Sets the <see cref="Template.FileSystem"/> property with a properly configured
    ///     <see cref="DatabaseFileSystem"/> implementation.
    ///   </para>
    ///   <para>
    ///     Its behavior is the same as with the Local File System, except this uses a SQL
    ///     database table instead of directories and files.
    ///   </para>
    ///   <para>Example:</para>
    ///   <para>var fileSystem = new DatabaseFileSystem(...);</para>
    ///   <para>fileSystem.FullPath("mypartial"); // "_mypartial"</para>
    ///   <para>fileSystem.FullPath("dir/mypartial"); // "dir/_mypartial"</para>
    /// </summary>
    /// <param name="dbProviderFactory">Database provider factory.</param>
    /// <param name="connectionString">Connection string.</param>
    /// <param name="schemaName">
    ///   Name of the SQL schema which contains the mail templates table.
    /// </param>
    /// <param name="tableName">Mail templates table name.</param>
    /// <param name="parameterPrefix">SQL command parameter prefix.</param>
    /// <param name="pathColumnName">Path column name.</param>
    /// <param name="templateColumnName">Template column name.</param>
    public DatabaseMailTemplateManager(
        DbProviderFactory dbProviderFactory,
        string connectionString,
        string schemaName = DatabaseFileSystem.Defaults.SchemaName,
        string tableName = DatabaseFileSystem.Defaults.TableName,
        string parameterPrefix = DatabaseFileSystem.Defaults.ParameterPrefix,
        string pathColumnName = DatabaseFileSystem.Defaults.PathColumnName,
        string templateColumnName = DatabaseFileSystem.Defaults.TemplateColumnName)
    {
        var fs = new DatabaseFileSystem(
            dbProviderFactory, connectionString, schemaName, tableName,
            parameterPrefix, pathColumnName, templateColumnName);

        Template.FileSystem = fs;
        _databaseFileSystem = fs;
    }

    /// <inheritdoc/>
    protected override string MasterFullPath(string templatePath)
    {
        return DatabaseFileSystem.FullPath(templatePath, true);
    }

    /// <inheritdoc/>
    protected override string ReadMasterTemplateFile(string fullPath)
    {
        return _databaseFileSystem.ReadTemplateFileInternal(fullPath);
    }
}
