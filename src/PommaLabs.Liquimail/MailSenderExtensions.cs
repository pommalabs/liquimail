﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using AngleSharp.Dom;
using AngleSharp.Html.Parser;
using DotLiquid;
using MimeKit;
using PommaLabs.Liquimail.Core;
using PommaLabs.Liquimail.Resources;

namespace PommaLabs.Liquimail;

/// <summary>
///   Extension methods for <see cref="IMailSender"/>.
/// </summary>
public static class MailSenderExtensions
{
    /// <summary>
    ///   Simply sends given mail message, setting the rendered template as the mail body.
    /// </summary>
    /// <param name="mailSender">The mail sender.</param>
    /// <param name="mail">The message that should be sent.</param>
    /// <param name="templateName">The name of the template that should be used.</param>
    /// <param name="localVariables">The local variables container used to render the template.</param>
    /// <exception cref="ArgumentNullException">
    ///   Given mail message is null. Given local variables container is null.
    /// </exception>
    /// <exception cref="ArgumentException">
    ///   Given mail message has neither senders nor recipients. Given template name is null,
    ///   empty or blank.
    /// </exception>
    public static void SendMailFromTemplate(this IMailSender mailSender, MimeMessage mail, string templateName, Hash localVariables)
    {
        // Preconditions
        AbstractMailSender.CheckSendMail(mailSender, mail);
        CheckSendMailFromTemplate(templateName, localVariables);

        var template = mailSender.TemplateManager.LoadTemplate(templateName);
        var renderedTemplate = template.Render(localVariables);

        var bodyBuilder = new BodyBuilder
        {
            HtmlBody = renderedTemplate,
            TextBody = ConvertHtmlToMarkdown(renderedTemplate)
        };

        mail.Body = bodyBuilder.ToMessageBody();
        mailSender.SendMail(mail);
    }

    /// <summary>
    ///   Simply sends given mail message, setting the rendered template as the mail HTML body
    ///   and builder as mail body.
    /// </summary>
    /// <param name="mailSender">The mail sender.</param>
    /// <param name="mail">The message that should be sent.</param>
    /// <param name="bodyBuilder">The builder used to prepare the mail body.</param>
    /// <param name="templateName">The name of the template that should be used.</param>
    /// <param name="localVariables">The local variables container used to render the template.</param>
    /// <exception cref="ArgumentNullException">
    ///   Given mail message is null. Given body builder is null. Given local variables
    ///   container is null.
    /// </exception>
    /// <exception cref="ArgumentException">
    ///   Given mail message has neither senders nor recipients. Given template name is null,
    ///   empty or blank. Mail body is not null.
    /// </exception>
    public static void SendMailFromTemplate(this IMailSender mailSender, MimeMessage mail, BodyBuilder bodyBuilder, string templateName, Hash localVariables)
    {
        // Preconditions
        AbstractMailSender.CheckSendMail(mailSender, mail);
        CheckSendMailFromTemplate(templateName, localVariables);
        CheckSendMailWithBuilder(mail, bodyBuilder);

        var template = mailSender.TemplateManager.LoadTemplate(templateName);
        var renderedTemplate = template.Render(localVariables);

        bodyBuilder.HtmlBody = renderedTemplate;
        bodyBuilder.TextBody ??= ConvertHtmlToMarkdown(renderedTemplate);

        mail.Body = bodyBuilder.ToMessageBody();
        mailSender.SendMail(mail);
    }

    /// <summary>
    ///   Simply sends given mail message.
    /// </summary>
    /// <param name="mailSender">The mail sender.</param>
    /// <param name="mail">The message that should be sent.</param>
    /// <param name="templateName">The name of the template that should be used.</param>
    /// <param name="localVariables">The local variables container used to render the template.</param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <exception cref="ArgumentNullException">
    ///   Given mail message is null. Given local variables container is null.
    /// </exception>
    /// <exception cref="ArgumentException">
    ///   Given mail message has neither senders nor recipients. Given template name is null,
    ///   empty or blank.
    /// </exception>
    public static async Task SendMailFromTemplateAsync(this IMailSender mailSender, MimeMessage mail, string templateName, Hash localVariables, CancellationToken cancellationToken = default)
    {
        // Preconditions
        AbstractMailSender.CheckSendMail(mailSender, mail);
        CheckSendMailFromTemplate(templateName, localVariables);

        var template = mailSender.TemplateManager.LoadTemplate(templateName);
        var renderedTemplate = template.Render(localVariables);

        var bodyBuilder = new BodyBuilder
        {
            HtmlBody = renderedTemplate,
            TextBody = ConvertHtmlToMarkdown(renderedTemplate)
        };

        mail.Body = bodyBuilder.ToMessageBody();
        await mailSender.SendMailAsync(mail, cancellationToken).ConfigureAwait(false);
    }

    /// <summary>
    ///   Simply sends given mail message, setting the rendered template as the mail HTML body
    ///   and builder as mail body.
    /// </summary>
    /// <param name="mailSender">The mail sender.</param>
    /// <param name="mail">The message that should be sent.</param>
    /// <param name="bodyBuilder">The builder used to prepare the mail body.</param>
    /// <param name="templateName">The name of the template that should be used.</param>
    /// <param name="localVariables">The local variables container used to render the template.</param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <exception cref="ArgumentNullException">
    ///   Given mail message is null. Given body builder is null. Given local variables
    ///   container is null.
    /// </exception>
    /// <exception cref="ArgumentException">
    ///   Given mail message has neither senders nor recipients. Given template name is null,
    ///   empty or blank. Mail body is not null.
    /// </exception>
    public static async Task SendMailFromTemplateAsync(this IMailSender mailSender, MimeMessage mail, BodyBuilder bodyBuilder, string templateName, Hash localVariables, CancellationToken cancellationToken = default)
    {
        // Preconditions
        AbstractMailSender.CheckSendMail(mailSender, mail);
        CheckSendMailFromTemplate(templateName, localVariables);
        CheckSendMailWithBuilder(mail, bodyBuilder);

        var template = mailSender.TemplateManager.LoadTemplate(templateName);
        var renderedTemplate = template.Render(localVariables);

        bodyBuilder.HtmlBody = renderedTemplate;
        bodyBuilder.TextBody ??= ConvertHtmlToMarkdown(renderedTemplate);

        mail.Body = bodyBuilder.ToMessageBody();
        await mailSender.SendMailAsync(mail, cancellationToken).ConfigureAwait(false);
    }

    private static string ConvertHtmlToMarkdown(string renderedTemplate)
    {
        var innerText = new HtmlParser().ParseDocument(renderedTemplate).Body.Text();

        // Strip multiple blank characters.
        innerText = Regex.Replace(innerText, "[^\\S\\r\\n]+", " ", default, Constants.RegexMatchTimeout);

        // Strip multiple newline characters.
        innerText = Regex.Replace(innerText, "([\\r\\n]\\s*[\\r\\n])+", Environment.NewLine + Environment.NewLine, default, Constants.RegexMatchTimeout);

        return innerText;
    }

    #region Preconditions

    /// <summary>
    ///   Integrity checks for send methods which include a template.
    /// </summary>
    /// <param name="templateName">The name of the template that should be used.</param>
    /// <param name="localVariables">The local variables container used to render the template.</param>
    internal static void CheckSendMailFromTemplate(this string templateName, Hash localVariables)
    {
        if (string.IsNullOrWhiteSpace(templateName)) throw new ArgumentException(ErrorMessages.MailSender_MissingTemplateName, nameof(templateName));
        if (localVariables == null) throw new ArgumentNullException(nameof(localVariables), ErrorMessages.MailSender_NullLocalVariables);
    }

    /// <summary>
    ///   Integrity checks for send methods which include a body builder.
    /// </summary>
    /// <param name="mailMessage">The message that should be sent.</param>
    /// <param name="bodyBuilder">The builder used to prepare the mail body.</param>
    internal static void CheckSendMailWithBuilder(this MimeMessage mailMessage, BodyBuilder bodyBuilder)
    {
        if (mailMessage.Body != null) throw new ArgumentNullException(nameof(mailMessage), ErrorMessages.MailSender_NotNullMailBody);
        if (bodyBuilder == null) throw new ArgumentNullException(nameof(bodyBuilder), ErrorMessages.MailSender_NullBodyBuilder);
    }

    #endregion Preconditions
}
