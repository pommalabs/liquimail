﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Threading;
using System.Threading.Tasks;
using MimeKit;
using PommaLabs.Liquimail.Templating;

namespace PommaLabs.Liquimail;

/// <summary>
///   Represents an object whose only goal is to send mail messages.
/// </summary>
public interface IMailSender
{
    /// <summary>
    ///   The manager used to retrieve the specified templates.
    /// </summary>
    IMailTemplateManager TemplateManager { get; }

    /// <summary>
    ///   Simply sends given mail message.
    /// </summary>
    /// <param name="mailMessage">The message that should be sent.</param>
    /// <exception cref="ArgumentNullException">Given mail message is null.</exception>
    /// <exception cref="ArgumentException">
    ///   Given mail message has neither senders nor recipients.
    /// </exception>
    void SendMail(MimeMessage mailMessage);

    /// <summary>
    ///   Simply sends given mail message.
    /// </summary>
    /// <param name="mailMessage">The message that should be sent.</param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <exception cref="ArgumentNullException">Given mail message is null.</exception>
    /// <exception cref="ArgumentException">
    ///   Given mail message has neither senders nor recipients.
    /// </exception>
    Task SendMailAsync(MimeMessage mailMessage, CancellationToken cancellationToken = default);

    /// <summary>
    ///   Tries the connection to the mail server and it disconnects immediately. If an error
    ///   occurs, an exception is thrown.
    /// </summary>
    /// <remarks>This method is used by Liquimail health check.</remarks>
    void TryConnect();

    /// <summary>
    ///   Tries the connection to the mail server and it disconnects immediately. If an error
    ///   occurs, an exception is thrown.
    /// </summary>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    /// <remarks>This method is used by Liquimail health check.</remarks>
    Task TryConnectAsync(CancellationToken cancellationToken = default);
}
