﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Diagnostics;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MailKit;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Logging;
using MimeKit;
using PommaLabs.Liquimail.Core;
using PommaLabs.Liquimail.Resources;
using PommaLabs.Liquimail.Templating;

namespace PommaLabs.Liquimail;

/// <summary>
///   A function which asynchronously builds a valid SMTP client, already connected and
///   authenticated to the SMTP server. The <see cref="SmtpMailSender"/> class expects this
///   function to behave this way.
/// </summary>
/// <returns>A valid SMTP client.</returns>
public delegate Task<SmtpClient> AsyncSmtpClientBuilder(CancellationToken cancellationToken);

/// <summary>
///   A function which builds a valid SMTP client, already connected and authenticated to the
///   SMTP server. The <see cref="SmtpMailSender"/> class expects this function to behave this way.
/// </summary>
/// <returns>A valid SMTP client.</returns>
public delegate SmtpClient SmtpClientBuilder();

/// <summary>
///   Really sends mail messages using an SMTP client, which must be configured through the
///   constructors exposed by this class.
/// </summary>
public sealed class SmtpMailSender : AbstractMailSender
{
    /// <summary>
    ///   The logger used internally by the mail sender.
    /// </summary>
    private readonly ILogger<SmtpMailSender> _logger;

    /// <summary>
    ///   Builds a mail sender which will open an SMTP client using specified parameters.
    /// </summary>
    /// <param name="parameters">
    ///   The parameters which must be used to configure the SMTP client.
    /// </param>
    /// <param name="templateManager">The template manager.</param>
    /// <param name="logger">The logger.</param>
    /// <remarks>
    ///   <para>
    ///     This constructor creates a default <see cref="AsyncSmtpClientBuilder"/> which
    ///     connects to the SMTP server according to given parameters.
    ///   </para>
    ///   <para>
    ///     In particular, user is authenticated if and only if an user name has been specified
    ///     and the XOAUTH2 mechanism is removed by default.
    ///   </para>
    ///   <para>
    ///     If the default behavior does not suit you, then you may use the constructor in which
    ///     you can directly specify the client builder definition.
    ///   </para>
    /// </remarks>
    public SmtpMailSender(SmtpClientParameters parameters, IMailTemplateManager templateManager, ILogger<SmtpMailSender> logger)
        : base(templateManager)
    {
        // Preconditions
        ArgumentNullException.ThrowIfNull(parameters);
        if (string.IsNullOrWhiteSpace(parameters.Host)) throw new ArgumentException(ErrorMessages.MailSender_MissingHost, nameof(parameters));
        if (parameters.Port < 0) throw new ArgumentOutOfRangeException(nameof(parameters));
        if (!Enum.IsDefined(parameters.SecureSocketOptions)) throw new ArgumentException(ErrorMessages.MailSender_InvalidSslOptions, nameof(parameters));

        _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        var protocolLogger = GetProtocolLogger(parameters, logger);

        SmtpClientBuilder = () =>
        {
            using (StartSmtpConnectionActivity(parameters))
            {
                _logger.LogTrace("SMTP client is connecting to host {SmtpHost}...", parameters.Host);
                var smtpClient = new SmtpClient(protocolLogger);

                if (parameters.DisableServerCertificateValidation)
                {
#pragma warning disable CA5359 // Do Not Disable Certificate Validation
#pragma warning disable S4830 // Server certificates should be verified during SSL/TLS connections
                    // CA5359 warning is disabled because in this case library user explicitly
                    // configured the parameters in order to disable certificate validation.
                    _logger.LogTrace("Server certificate validation for host {SmtpHost} has been disabled", parameters.Host);
                    smtpClient.ServerCertificateValidationCallback = (_, _, _, _) => true;
#pragma warning restore S4830 // Server certificates should be verified during SSL/TLS connections
#pragma warning restore CA5359 // Do Not Disable Certificate Validation
                }

                smtpClient.Connect(parameters.Host, parameters.Port, parameters.SecureSocketOptions);
                _logger.LogTrace("SMTP client is connected to host {SmtpHost}", parameters.Host);

                // Note: since we don't have an OAuth2 token, disable the XOAUTH2 authentication mechanism.
                smtpClient.AuthenticationMechanisms.Remove("XOAUTH2");

                // If user has specified an user name, perform authentication.
                if (!string.IsNullOrWhiteSpace(parameters.UserName))
                {
                    _logger.LogTrace("SMTP client is authenticating with user name {SmtpUserName}...", parameters.UserName);
                    var credentials = new NetworkCredential(parameters.UserName, parameters.Password);
                    smtpClient.Authenticate(credentials);
                    _logger.LogTrace("SMTP client is authenticated with user name {SmtpUserName}", parameters.UserName);
                }
                return smtpClient;
            }
        };

        AsyncSmtpClientBuilder = async (cancellationToken) =>
        {
            using (StartSmtpConnectionActivity(parameters))
            {
                _logger.LogTrace("SMTP client is connecting to host {SmtpHost}...", parameters.Host);
                var smtpClient = new SmtpClient(protocolLogger);

                if (parameters.DisableServerCertificateValidation)
                {
#pragma warning disable CA5359 // Do Not Disable Certificate Validation
#pragma warning disable S4830 // Server certificates should be verified during SSL/TLS connections
                    // CA5359 warning is disabled because in this case library user explicitly
                    // configured the parameters in order to disable certificate validation.
                    _logger.LogTrace("Server certificate validation for host {SmtpHost} has been disabled", parameters.Host);
                    smtpClient.ServerCertificateValidationCallback = (_, _, _, _) => true;
#pragma warning restore S4830 // Server certificates should be verified during SSL/TLS connections
#pragma warning restore CA5359 // Do Not Disable Certificate Validation
                }

                await smtpClient.ConnectAsync(parameters.Host, parameters.Port, parameters.SecureSocketOptions, cancellationToken).ConfigureAwait(false);
                _logger.LogTrace("SMTP client is connected to host {SmtpHost}", parameters.Host);

                // Note: since we don't have an OAuth2 token, disable the XOAUTH2 authentication mechanism.
                smtpClient.AuthenticationMechanisms.Remove("XOAUTH2");

                // If user has specified an user name, perform authentication.
                if (!string.IsNullOrWhiteSpace(parameters.UserName))
                {
                    _logger.LogTrace("SMTP client is authenticating with user name {SmtpUserName}...", parameters.UserName);
                    var credentials = new NetworkCredential(parameters.UserName, parameters.Password);
                    await smtpClient.AuthenticateAsync(credentials, cancellationToken).ConfigureAwait(false);
                    _logger.LogTrace("SMTP client is authenticated with user name {SmtpUserName}", parameters.UserName);
                }
                return smtpClient;
            }
        };
    }

    /// <summary>
    ///   Builds a mail sender which will get valid SMTP clients through the specified function.
    /// </summary>
    /// <param name="clientBuilder">The function that will be used to get valid SMTP clients.</param>
    /// <param name="templateManager">The template manager.</param>
    /// <param name="logger">The logger.</param>
    public SmtpMailSender(SmtpClientBuilder clientBuilder, IMailTemplateManager templateManager, ILogger<SmtpMailSender> logger)
        : base(templateManager)
    {
        _logger = logger ?? throw new ArgumentNullException(nameof(logger));

        SmtpClientBuilder = clientBuilder ?? throw new ArgumentNullException(nameof(clientBuilder));
        AsyncSmtpClientBuilder = _ => Task.FromResult(clientBuilder?.Invoke());
    }

    /// <summary>
    ///   Builds a mail sender which will get valid SMTP clients through the specified function.
    /// </summary>
    /// <param name="clientBuilder">The function that will be used to get valid SMTP clients.</param>
    /// <param name="templateManager">The template manager.</param>
    /// <param name="logger">The logger.</param>
    public SmtpMailSender(AsyncSmtpClientBuilder clientBuilder, IMailTemplateManager templateManager, ILogger<SmtpMailSender> logger)
        : base(templateManager)
    {
        _logger = logger ?? throw new ArgumentNullException(nameof(logger));

        SmtpClientBuilder = () => clientBuilder?.Invoke(CancellationToken.None).Result;
        AsyncSmtpClientBuilder = clientBuilder ?? throw new ArgumentNullException(nameof(clientBuilder));
    }

    /// <summary>
    ///   The <see langword="async"/> SMTP client builder used to send mail messages.
    /// </summary>
    public AsyncSmtpClientBuilder AsyncSmtpClientBuilder { get; }

    /// <summary>
    ///   The SMTP client builder used to send mail messages.
    /// </summary>
    public SmtpClientBuilder SmtpClientBuilder { get; }

    /// <inheritdoc/>
    public override void TryConnect()
    {
        using var client = SmtpClientBuilder.Invoke();
        client?.Disconnect(true);
    }

    /// <inheritdoc/>
    public override async Task TryConnectAsync(CancellationToken cancellationToken = default)
    {
        using var client = await AsyncSmtpClientBuilder.Invoke(cancellationToken).ConfigureAwait(false);
        await client.DisconnectAsync(true, cancellationToken).ConfigureAwait(false);
    }

    /// <inheritdoc/>
    protected override void SendMailInternal(MimeMessage mailMessage)
    {
        using var client = SmtpClientBuilder.Invoke();

        using (StartSendMailMessageActivity(mailMessage))
        {
            _logger.LogTrace("SMTP client is sending a mail message from \"{MailFrom}\" to \"{MailTo}\"...", mailMessage.From, mailMessage.To);
            client.Send(mailMessage);
            _logger.LogTrace("SMTP client has sent a mail message from \"{MailFrom}\" to \"{MailTo}\"", mailMessage.From, mailMessage.To);

            _logger.LogTrace("SMTP client is disconnecting...");
            client.Disconnect(true);
            _logger.LogTrace("SMTP client is disconnected");
        }
    }

    /// <inheritdoc/>
    protected override async Task SendMailInternalAsync(MimeMessage mailMessage, CancellationToken cancellationToken)
    {
        using var client = await AsyncSmtpClientBuilder.Invoke(cancellationToken).ConfigureAwait(false);

        using (StartSendMailMessageActivity(mailMessage))
        {
            _logger.LogTrace("SMTP client is sending a mail message from \"{MailFrom}\" to \"{MailTo}\"...", mailMessage.From, mailMessage.To);
            await client.SendAsync(mailMessage, cancellationToken).ConfigureAwait(false);
            _logger.LogTrace("SMTP client has sent a mail message from \"{MailFrom}\" to \"{MailTo}\"", mailMessage.From, mailMessage.To);

            _logger.LogTrace("SMTP client is disconnecting...");
            await client.DisconnectAsync(true, cancellationToken).ConfigureAwait(false);
            _logger.LogTrace("SMTP client is disconnected");
        }
    }

    private static IProtocolLogger GetProtocolLogger(SmtpClientParameters parameters, ILogger<SmtpMailSender> logger)
    {
        var protocolLoggerParams = parameters.ProtocolLogger;
        return protocolLoggerParams.Enabled ? new LiquimailProtocolLogger(logger, protocolLoggerParams.RedactSecrets) : new NullProtocolLogger();
    }

    private static Activity StartSendMailMessageActivity(MimeMessage mailMessage)
    {
        var activity = LiquimailActivitySource.Instance.StartActivity(ActivityNames.MailMessageSending, ActivityKind.Client);
        if (activity != null)
        {
            activity.AddTag("mail.from", mailMessage.From);
            activity.AddTag("mail.to", mailMessage.To);
        }
        return activity;
    }

    private static Activity StartSmtpConnectionActivity(SmtpClientParameters parameters)
    {
        var activity = LiquimailActivitySource.Instance.StartActivity(ActivityNames.SmtpConnection, ActivityKind.Client);
        if (activity != null)
        {
            activity.AddTag("net.peer.name", parameters.Host);
            activity.AddTag("net.peer.port", parameters.Port);
        }
        return activity;
    }
}
