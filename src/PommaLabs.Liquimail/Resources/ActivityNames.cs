﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Liquimail.Resources;

internal static class ActivityNames
{
    public const string MailMessageSending = "mail_message_sending";
    public const string SmtpConnection = "smtp_connection";
}
