﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Liquimail.Resources;

internal static class ErrorMessages
{
    public const string LRUCache_InvalidCapacity = "Capacity should be greater than or equal to 1";

    public const string MailSender_InvalidSslOptions = "Invalid SSL options";
    public const string MailSender_MissingFrom = "At least one sender should be specified";
    public const string MailSender_MissingHost = "Host has not been specified";
    public const string MailSender_MissingTemplateName = "Template name must be specified";
    public const string MailSender_MissingTo = "At least one recipient should be specified";
    public const string MailSender_NotNullMailBody = "When using a body builder and a template, mail body should not be specified, since it would be overwritten while applying the rendered template";
    public const string MailSender_NullBodyBuilder = "Body builder cannot be null";
    public const string MailSender_NullLocalVariables = "Local variables container cannot be null";
    public const string MailSender_NullMailSender = "Mail sender cannot be null";
    public const string MailSender_NullMimeMessage = "Mime message cannot be null";

    public const string MailTemplateManager_IllegalTemplateName = "Illegal template name \"{0}\"";
    public const string MailTemplateManager_MissingAbsolutePath = "Absolute path must be specified";
    public const string MailTemplateManager_MissingConnectionString = "Connection string must be specified";
    public const string MailTemplateManager_MissingRootNamespace = "Root namespace must be specified";
    public const string MailTemplateManager_NotInitialized = "Manager must be initialized by calling SetTemplatesPath or SetTemplatesAssembly";
    public const string MailTemplateManager_NotSupported = "Current configuration does not support templates management";
    public const string MailTemplateManager_NullAssembly = "Specified assembly cannot be null";
    public const string MailTemplateManager_NullDbProviderFactory = "Specified DB provider factory cannot be null";
    public const string MailTemplateManager_TemplatePathNotFound = "Specified template path \"{0}\" was not found";
}
