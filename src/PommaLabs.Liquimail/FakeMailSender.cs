﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MimeKit;
using PommaLabs.Liquimail.Templating;

namespace PommaLabs.Liquimail;

/// <summary>
///   A fake mail sender, which put all messages into an in-memory list.
/// </summary>
public sealed class FakeMailSender : AbstractMailSender
{
    /// <summary>
    ///   Builds the mock mail sender using the specified dependencies.
    /// </summary>
    /// <param name="templateManager">The template manager.</param>
    public FakeMailSender(IMailTemplateManager templateManager)
        : base(templateManager)
    {
    }

    /// <summary>
    ///   All mail messages which have been sent through this instance.
    /// </summary>
    public IList<MimeMessage> SentMailMessages { get; } = new List<MimeMessage>();

    /// <summary>
    ///   Resets the mock and clears the list of sent mail messages.
    /// </summary>
    public void Reset()
    {
        // Clear sent mails, since in the initial state there are no messages.
        SentMailMessages.Clear();
    }

    /// <inheritdoc/>
    public override void TryConnect()
    {
    }

    /// <inheritdoc/>
    public override Task TryConnectAsync(CancellationToken cancellationToken = default)
    {
        return Task.CompletedTask;
    }

    /// <inheritdoc/>
    protected override void SendMailInternal(MimeMessage mailMessage)
    {
        SentMailMessages.Add(mailMessage);
    }

    /// <inheritdoc/>
    protected override Task SendMailInternalAsync(MimeMessage mailMessage, CancellationToken cancellationToken)
    {
        SentMailMessages.Add(mailMessage);
        return Task.CompletedTask;
    }
}
