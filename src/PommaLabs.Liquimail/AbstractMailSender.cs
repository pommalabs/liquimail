﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Threading;
using System.Threading.Tasks;
using MimeKit;
using PommaLabs.Liquimail.Resources;
using PommaLabs.Liquimail.Templating;

namespace PommaLabs.Liquimail;

/// <summary>
///   A base class from which one can start implementing its own mail sender.
/// </summary>
public abstract class AbstractMailSender : IMailSender
{
    /// <summary>
    ///   Sets the internal dependencies for the mail sender.
    /// </summary>
    /// <param name="templateManager">The template manager.</param>
    protected AbstractMailSender(IMailTemplateManager templateManager)
    {
        TemplateManager = templateManager ?? throw new ArgumentNullException(nameof(templateManager));
    }

    /// <inheritdoc/>
    public IMailTemplateManager TemplateManager { get; }

    /// <inheritdoc/>
    public void SendMail(MimeMessage mailMessage)
    {
        // Preconditions
        CheckSendMail(this, mailMessage);

        SendMailInternal(mailMessage);
    }

    /// <inheritdoc/>
    public async Task SendMailAsync(MimeMessage mailMessage, CancellationToken cancellationToken = default)
    {
        // Preconditions
        CheckSendMail(this, mailMessage);

        await SendMailInternalAsync(mailMessage, cancellationToken).ConfigureAwait(false);
    }

    /// <inheritdoc/>
    public abstract void TryConnect();

    /// <inheritdoc/>
    public abstract Task TryConnectAsync(CancellationToken cancellationToken = default);

    /// <summary>
    ///   Integrity checks for <see cref="SendMail"/>.
    /// </summary>
    /// <param name="mailSender">The mail sender.</param>
    /// <param name="mail">The message that should be sent.</param>
    internal static void CheckSendMail(IMailSender mailSender, MimeMessage mail)
    {
        if (mailSender == null) throw new ArgumentNullException(nameof(mailSender), ErrorMessages.MailSender_NullMailSender);
        if (mail == null) throw new ArgumentNullException(nameof(mail), ErrorMessages.MailSender_NullMimeMessage);
        if (mail.From.Count == 0) throw new ArgumentException(ErrorMessages.MailSender_MissingFrom, nameof(mail));
        if (mail.To.Count == 0) throw new ArgumentException(ErrorMessages.MailSender_MissingTo, nameof(mail));
    }

    /// <summary>
    ///   Simply sends given mail message.
    /// </summary>
    /// <param name="mailMessage">The message that should be sent.</param>
    protected abstract void SendMailInternal(MimeMessage mailMessage);

    /// <summary>
    ///   Simply sends given mail message.
    /// </summary>
    /// <param name="mailMessage">The message that should be sent.</param>
    /// <param name="cancellationToken">An optional cancellation token.</param>
    protected abstract Task SendMailInternalAsync(MimeMessage mailMessage, CancellationToken cancellationToken);
}
