﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using PommaLabs.Liquimail.Resources;

namespace PommaLabs.Liquimail.Core;

/// <summary>
///   A simple concurrent LRU cache, taken from this StackOverflow question: https://stackoverflow.com/questions/754233/is-it-there-any-lru-implementation-of-idictionary
/// </summary>
/// <typeparam name="TKey">The type of the key.</typeparam>
/// <typeparam name="TValue">The type of the value.</typeparam>
[SuppressMessage("Minor Code Smell", "S101:Types should be named in PascalCase", Justification = "Class is part of the public API surface and it should not be renamed")]
public sealed class ConcurrentLRUCache<TKey, TValue>
{
    private readonly Dictionary<TKey, LinkedListNode<LRUCacheItem>> _cacheMap = new();
    private readonly int _capacity;
    private readonly LinkedList<LRUCacheItem> _lruList = new();

    /// <summary>
    ///   Builds an LRU cache of given capacity.
    /// </summary>
    /// <param name="capacity">The maximum capacity of the LRU cache.</param>
    public ConcurrentLRUCache(int capacity)
    {
        // Preconditions
        if (capacity < 1) throw new ArgumentOutOfRangeException(nameof(capacity), ErrorMessages.LRUCache_InvalidCapacity);

        _capacity = capacity;
    }

    /// <summary>
    ///   The number of items contained in the LRU cache.
    /// </summary>
    public int Count => _cacheMap.Count;

    /// <summary>
    ///   Adds the specified key/value pair to the LRU cache. If this addition would make
    ///   collection bigger than capacity, then the least recently used item (LRU) is removed.
    /// </summary>
    /// <param name="key">Key.</param>
    /// <param name="val">Value.</param>
    public void Add(TKey key, TValue val)
    {
        lock (_cacheMap)
        {
            if (_cacheMap.Count >= _capacity)
            {
                RemoveFirst();
            }

            var cacheItem = new LRUCacheItem(key, val);
            var newNode = new LinkedListNode<LRUCacheItem>(cacheItem);

            if (_cacheMap.TryGetValue(key, out var existingNode))
            {
                // If key already exists, then node in priority queue should be temporarily
                // removed and below it will be added again with the new value.
                _lruList.Remove(existingNode);
                _cacheMap[key] = newNode;
            }
            else
            {
                // Key does not exist, so it should be added to the map.
                _cacheMap.Add(key, newNode);
            }

            // In any case, new node should be added to the priority list.
            _lruList.AddLast(newNode);
        }
    }

    /// <summary>
    ///   Tries to read the value linked to specified key.
    /// </summary>
    /// <param name="key">Key.</param>
    /// <param name="value">Value.</param>
    /// <returns>True if value has been found, false otherwise.</returns>
    public bool TryGetValue(TKey key, out TValue value)
    {
        lock (_cacheMap)
        {
            if (_cacheMap.TryGetValue(key, out var node))
            {
                value = node.Value.Value;
                _lruList.Remove(node);
                _lruList.AddLast(node);
                return true;
            }

            value = default;
            return false;
        }
    }

    private void RemoveFirst()
    {
        // Removes from LRU priority list.
        var node = _lruList.First;
        _lruList.RemoveFirst();

        // Removes from cache.
        _cacheMap.Remove(node.Value.Key);
    }

    private readonly struct LRUCacheItem : IEquatable<LRUCacheItem>
    {
        public LRUCacheItem(TKey key, TValue value)
        {
            Key = key;
            Value = value;
        }

        public TKey Key { get; }

        public TValue Value { get; }

        public static bool operator !=(LRUCacheItem x, LRUCacheItem y) => !x.Equals(y);

        public static bool operator ==(LRUCacheItem x, LRUCacheItem y) => x.Equals(y);

        public bool Equals(LRUCacheItem other) => EqualityComparer<TKey>.Default.Equals(Key, other.Key);

        public override bool Equals(object obj) => obj is LRUCacheItem other && Equals(other);

        public override int GetHashCode()
        {
            unchecked
            {
                const int Prime = -1521134295;
                const int Hash = 12345701;
                return (Hash * Prime) + EqualityComparer<TKey>.Default.GetHashCode(Key);
            }
        }
    }
}
