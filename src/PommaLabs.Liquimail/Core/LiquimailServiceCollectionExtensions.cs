﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.Logging;
using PommaLabs.Liquimail;
using PommaLabs.Liquimail.Templating;

namespace Microsoft.Extensions.DependencyInjection;

/// <summary>
///   Registrations for core Liquimail services.
/// </summary>
public static class LiquimailServiceCollectionExtensions
{
    /// <summary>
    ///   Registers <see cref="DatabaseMailTemplateManager"/> as singleton for <see cref="IMailTemplateManager"/>.
    /// </summary>
    /// <param name="services">Service collection.</param>
    /// <param name="dbProviderFactory">Database provider factory.</param>
    /// <param name="connectionString">Connection string.</param>
    /// <param name="schemaName">
    ///   Name of the SQL schema which contains the mail templates table.
    /// </param>
    /// <param name="tableName">Mail templates table name.</param>
    /// <param name="parameterPrefix">SQL command parameter prefix.</param>
    /// <param name="pathColumnName">Path column name.</param>
    /// <param name="templateColumnName">Template column name.</param>
    /// <returns>Modified services collection.</returns>
    public static IServiceCollection AddDatabaseMailTemplateManager(
        this IServiceCollection services,
        DbProviderFactory dbProviderFactory,
        string connectionString,
        string schemaName = DatabaseFileSystem.Defaults.SchemaName,
        string tableName = DatabaseFileSystem.Defaults.TableName,
        string parameterPrefix = DatabaseFileSystem.Defaults.ParameterPrefix,
        string pathColumnName = DatabaseFileSystem.Defaults.PathColumnName,
        string templateColumnName = DatabaseFileSystem.Defaults.TemplateColumnName)
    {
        var databaseMailTemplateManager = new DatabaseMailTemplateManager(
            dbProviderFactory, connectionString, schemaName, tableName,
            parameterPrefix, pathColumnName, templateColumnName);

        return services?.AddOrReplaceSingleton<IMailTemplateManager>(databaseMailTemplateManager);
    }

    /// <summary>
    ///   Registers <see cref="EmbeddedMailTemplateManager"/> as singleton for <see cref="IMailTemplateManager"/>.
    /// </summary>
    /// <param name="services">Service collection.</param>
    /// <param name="assembly">The assembly in which all templates are stored.</param>
    /// <param name="rootNamespace">
    ///   The root namespace which will be used as prefix for all paths.
    /// </param>
    /// <returns>Modified services collection.</returns>
    public static IServiceCollection AddEmbeddedMailTemplateManager(this IServiceCollection services, Assembly assembly, string rootNamespace)
        => services?.AddOrReplaceSingleton<IMailTemplateManager>(new EmbeddedMailTemplateManager(assembly, rootNamespace));

    /// <summary>
    ///   Registers <see cref="LocalMailTemplateManager"/> as singleton for <see cref="IMailTemplateManager"/>.
    /// </summary>
    /// <param name="services">Service collection.</param>
    /// <param name="absolutePath">The path in which all templates are stored.</param>
    /// <returns>Modified services collection.</returns>
    public static IServiceCollection AddLocalMailTemplateManager(this IServiceCollection services, string absolutePath)
        => services?.AddOrReplaceSingleton<IMailTemplateManager>(new LocalMailTemplateManager(absolutePath));

    /// <summary>
    ///   <para>
    ///     Registers <see cref="FakeMailSender"/> as singleton for <see cref="IMailSender"/>.
    ///     Sender is registered using <see cref="NotSupportedMailTemplateManager.Instance"/> as
    ///     default template manager.
    ///   </para>
    ///   <para>
    ///     Template manager can be changed by calling a specific registration method, like
    ///     <see cref="AddEmbeddedMailTemplateManager(IServiceCollection, Assembly, string)"/>,
    ///     which load templates from an assembly, or
    ///     <see cref="AddLocalMailTemplateManager(IServiceCollection, string)"/>, which loads
    ///     templates from local file system.
    ///   </para>
    /// </summary>
    /// <param name="services">Service collection.</param>
    /// <returns>Modified services collection.</returns>
    public static IServiceCollection AddFakeMailSender(this IServiceCollection services)
    {
        RegisterNotSupportedMailTemplateManagerIfNeeded(services);
        return services?.AddOrReplaceSingleton<IMailSender>(sp => new FakeMailSender(sp.GetService<IMailTemplateManager>()));
    }

    /// <summary>
    ///   <para>
    ///     Registers <see cref="SmtpMailSender"/> as singleton for <see cref="IMailSender"/>.
    ///     Sender is registered using specified parameters and
    ///     <see cref="NotSupportedMailTemplateManager.Instance"/> as default template manager.
    ///   </para>
    ///   <para>
    ///     Template manager can be changed by calling a specific registration method, like
    ///     <see cref="AddEmbeddedMailTemplateManager(IServiceCollection, Assembly, string)"/>,
    ///     which load templates from an assembly, or
    ///     <see cref="AddLocalMailTemplateManager(IServiceCollection, string)"/>, which loads
    ///     templates from local file system.
    ///   </para>
    /// </summary>
    /// <param name="services">Service collection.</param>
    /// <param name="parameters">SMTP client parameters.</param>
    /// <returns>Modified services collection.</returns>
    public static IServiceCollection AddSmtpMailSender(this IServiceCollection services, SmtpClientParameters parameters)
        => services.AddSmtpMailSender(_ => parameters);

    /// <summary>
    ///   <para>
    ///     Registers <see cref="SmtpMailSender"/> as singleton for <see cref="IMailSender"/>.
    ///     Sender is registered using specified parameters and
    ///     <see cref="NotSupportedMailTemplateManager.Instance"/> as default template manager.
    ///   </para>
    ///   <para>
    ///     Template manager can be changed by calling a specific registration method, like
    ///     <see cref="AddEmbeddedMailTemplateManager(IServiceCollection, Assembly, string)"/>,
    ///     which load templates from an assembly, or
    ///     <see cref="AddLocalMailTemplateManager(IServiceCollection, string)"/>, which loads
    ///     templates from local file system.
    ///   </para>
    /// </summary>
    /// <param name="services">Service collection.</param>
    /// <param name="parametersFactory">
    ///   A factory which allows building SMTP client parameters from a service provider.
    ///   It is useful when parameters are dynamic and need to be resolved from the container.
    /// </param>
    /// <returns>Modified services collection.</returns>
    public static IServiceCollection AddSmtpMailSender(this IServiceCollection services, Func<IServiceProvider, SmtpClientParameters> parametersFactory)
    {
        RegisterNotSupportedMailTemplateManagerIfNeeded(services);
        return services?.AddOrReplaceSingleton<IMailSender>(sp => new SmtpMailSender(
            parametersFactory?.Invoke(sp),
            sp.GetRequiredService<IMailTemplateManager>(),
            sp.GetRequiredService<ILogger<SmtpMailSender>>()));
    }

    /// <summary>
    ///   <para>
    ///     Registers <see cref="SmtpMailSender"/> as singleton for <see cref="IMailSender"/>.
    ///     Sender is registered using specified client builder and
    ///     <see cref="NotSupportedMailTemplateManager.Instance"/> as default template manager.
    ///   </para>
    ///   <para>
    ///     Template manager can be changed by calling a specific registration method, like
    ///     <see cref="AddEmbeddedMailTemplateManager(IServiceCollection, Assembly, string)"/>,
    ///     which load templates from an assembly, or
    ///     <see cref="AddLocalMailTemplateManager(IServiceCollection, string)"/>, which loads
    ///     templates from local file system.
    ///   </para>
    /// </summary>
    /// <param name="services">Service collection.</param>
    /// <param name="clientBuilder">The function that will be used to get valid SMTP clients.</param>
    /// <returns>Modified services collection.</returns>
    public static IServiceCollection AddSmtpMailSender(this IServiceCollection services, SmtpClientBuilder clientBuilder)
    {
        RegisterNotSupportedMailTemplateManagerIfNeeded(services);
        return services?.AddOrReplaceSingleton<IMailSender>(sp => new SmtpMailSender(
            clientBuilder,
            sp.GetService<IMailTemplateManager>(),
            sp.GetRequiredService<ILogger<SmtpMailSender>>()));
    }

    /// <summary>
    ///   <para>
    ///     Registers <see cref="SmtpMailSender"/> as singleton for <see cref="IMailSender"/>.
    ///     Sender is registered using specified client builder and
    ///     <see cref="NotSupportedMailTemplateManager.Instance"/> as default template manager.
    ///   </para>
    ///   <para>
    ///     Template manager can be changed by calling a specific registration method, like
    ///     <see cref="AddEmbeddedMailTemplateManager(IServiceCollection, Assembly, string)"/>,
    ///     which load templates from an assembly, or
    ///     <see cref="AddLocalMailTemplateManager(IServiceCollection, string)"/>, which loads
    ///     templates from local file system.
    ///   </para>
    /// </summary>
    /// <param name="services">Service collection.</param>
    /// <param name="clientBuilder">The function that will be used to get valid SMTP clients.</param>
    /// <returns>Modified services collection.</returns>
    public static IServiceCollection AddSmtpMailSender(this IServiceCollection services, AsyncSmtpClientBuilder clientBuilder)
    {
        RegisterNotSupportedMailTemplateManagerIfNeeded(services);
        return services?.AddOrReplaceSingleton<IMailSender>(sp => new SmtpMailSender(
            clientBuilder,
            sp.GetService<IMailTemplateManager>(),
            sp.GetRequiredService<ILogger<SmtpMailSender>>()));
    }

    private static IServiceCollection AddOrReplaceSingleton<TService>(this IServiceCollection services, Func<IServiceProvider, TService> implementationFactory)
        where TService : class
    {
        var d = services.FirstOrDefault(x => x.ServiceType == typeof(TService));
        if (d != null)
        {
            services.Remove(d);
        }
        return services.AddSingleton(implementationFactory);
    }

    private static IServiceCollection AddOrReplaceSingleton<TService>(this IServiceCollection services, TService singleton)
        where TService : class
    {
        var d = services.FirstOrDefault(x => x.ServiceType == typeof(TService));
        if (d != null)
        {
            services.Remove(d);
        }
        return services.AddSingleton(singleton);
    }

    private static void RegisterNotSupportedMailTemplateManagerIfNeeded(IServiceCollection services)
    {
        if (services.All(s => s.ServiceType != typeof(IMailTemplateManager)))
        {
            // Registers the default instance, which might later be changed into another one,
            // like Embedded or Local, by calling the specific registration method.
            services.AddSingleton<IMailTemplateManager>(NotSupportedMailTemplateManager.Instance);
        }
    }
}
