﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Text;
using MailKit;
using Microsoft.Extensions.Logging;

namespace PommaLabs.Liquimail.Core;

internal sealed class LiquimailProtocolLogger : IProtocolLogger
{
    private readonly ILogger _logger;
    private readonly bool _redactSecrets;

    public LiquimailProtocolLogger(ILogger logger, bool redactSecrets)
    {
        _logger = logger;
        _redactSecrets = redactSecrets;
    }

    public IAuthenticationSecretDetector AuthenticationSecretDetector { get; set; }

    public void Dispose()
    {
        GC.SuppressFinalize(this);
    }

    public void LogClient(byte[] buffer, int offset, int count)
    {
        var initialOffset = offset;
        var messageBuilder = new StringBuilder();
        if (_redactSecrets)
        {
            foreach (var secret in AuthenticationSecretDetector.DetectSecrets(buffer, offset, count))
            {
                if (secret.StartIndex > offset)
                {
                    messageBuilder.Append(Encoding.UTF8.GetString(buffer, offset, secret.StartIndex - offset));
                }
                offset = secret.StartIndex + secret.Length;
                messageBuilder.Append("********" /* Secret placeholder */);
            }
        }
        messageBuilder.Append(Encoding.UTF8.GetString(buffer, offset, count - (offset - initialOffset)));
        _logger.LogTrace("C: {Buffer}", messageBuilder.ToString());
    }

    public void LogConnect(Uri uri)
    {
        _logger.LogTrace("Connected to {SmtpHost}", uri);
    }

    public void LogServer(byte[] buffer, int offset, int count)
    {
        _logger.LogTrace("S: {Buffer}", Encoding.UTF8.GetString(buffer, offset, count));
    }
}
