﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using MailKit.Security;

namespace PommaLabs.Liquimail;

/// <summary>
///   The parameters which may be needed to build an SMTP client.
/// </summary>
public sealed class SmtpClientParameters
{
    /// <summary>
    ///   Disables server certificate validation. Defaults to false, which enables proper validation.
    /// </summary>
    public bool DisableServerCertificateValidation { get; set; }

    /// <summary>
    ///   Parameters related to protocol logger, which logs the communication
    ///   between the client and the server.
    /// </summary>
    public ProtocolLoggerParameters ProtocolLogger { get; } = new ProtocolLoggerParameters();

    /// <summary>
    ///   The host name to connect to.
    /// </summary>
    public string Host { get; set; }

    /// <summary>
    ///   The password.
    /// </summary>
    public string Password { get; set; }

    /// <summary>
    ///   The port to connect to. If the specified port is 0, then the default port will be used.
    /// </summary>
    public int Port { get; set; }

    /// <summary>
    ///   The secure socket options used when connecting. Defaults to <see cref="SecureSocketOptions.Auto"/>.
    /// </summary>
    public SecureSocketOptions SecureSocketOptions { get; set; } = SecureSocketOptions.Auto;

    /// <summary>
    ///   The user name.
    /// </summary>
    public string UserName { get; set; }
}

/// <summary>
///   Parameters related to protocol logger.
/// </summary>
public sealed class ProtocolLoggerParameters
{
    /// <summary>
    ///   Enables the protocol logger, which logs the communication between the client and the server.
    ///   Protocol logger should be enabled only for diagnosing communication issues,
    ///   as it degrades performance and it produces an important amount of log messages.
    ///   Defaults to false, which disables the protocol logger.
    /// </summary>
    public bool Enabled { get; set; }

    /// <summary>
    ///   Whether or not authentication secrets should be redacted. Defaults to true.
    /// </summary>
    public bool RedactSecrets { get; set; } = true;
}
