﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Internal;

[assembly: InternalsVisibleTo("PommaLabs.Liquimail.UnitTests")]

namespace PommaLabs.Liquimail.AspNetCore;

/// <summary>
///   Health check for Liquimail. This check verifies that SMTP server is healthy by trying to
///   connect to it. No mail message is sent, the health check simply connects and disconnects
///   from the SMTP server.
/// </summary>
public sealed class LiquimailHealthCheck : IHealthCheck
{
    private static readonly ConditionalWeakTable<IMailSender, CachedResult> s_cachedResults = new();
    private static readonly SystemClock s_systemClock = new();

    private readonly IMailSender _mailSender;
    private readonly ISystemClock _systemClock;

    /// <summary>
    ///   Builds the health check for Liquimail.
    /// </summary>
    /// <param name="mailSender">The mail sender.</param>
    /// <param name="serviceProvider">The service provider.</param>
    public LiquimailHealthCheck(IMailSender mailSender, IServiceProvider serviceProvider)
    {
        _mailSender = mailSender;
        _systemClock = (serviceProvider.GetService(typeof(ISystemClock)) as ISystemClock) ?? s_systemClock;
    }

    /// <inheritdoc/>
    public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
    {
        if (s_cachedResults.TryGetValue(_mailSender, out var cachedResult) && _systemClock.UtcNow < cachedResult.ExpiresAt)
        {
            return cachedResult.Result;
        }

        var data = new Dictionary<string, object>
        {
            ["CheckId"] = Guid.NewGuid()
        };

        try
        {
            await _mailSender.TryConnectAsync(cancellationToken).ConfigureAwait(false);

            // If we get here, everything is running fine.
            return AddResultToCache(HealthCheckResult.Healthy(data: data));
        }
        catch (Exception ex)
        {
            return AddResultToCache(HealthCheckResult.Unhealthy("SMTP server could not be reached", ex, data));
        }
    }

    internal static void ClearCachedResults()
    {
        s_cachedResults.Clear();
    }

    private HealthCheckResult AddResultToCache(HealthCheckResult result)
    {
        var cacheLifetime = TimeSpan.FromSeconds(result.Status == HealthStatus.Healthy ? 27 : 9);
        s_cachedResults.AddOrUpdate(_mailSender, new CachedResult(result, _systemClock.UtcNow + cacheLifetime));
        return result;
    }

    private sealed record CachedResult(HealthCheckResult Result, DateTimeOffset ExpiresAt);
}
