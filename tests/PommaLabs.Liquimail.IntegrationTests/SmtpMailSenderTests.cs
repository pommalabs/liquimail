﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using FakeItEasy;
using FakeItEasy.Core;
using Microsoft.Extensions.Logging;
using MimeKit;
using PommaLabs.Liquimail.Templating;

namespace PommaLabs.Liquimail.IntegrationTests;

[TestFixture, Parallelizable]
public class SmtpMailSenderTests
{
    private const int TryConnectMinimumTraceCount = 2;
    private const int SendMailMinimumTraceCount = 6;

    [Test]
    public void TryConnect_DefaultParameters_ShouldLogExpectedTraces()
    {
        // Arrange
        var logger = A.Fake<ILogger<SmtpMailSender>>();
        var templateManager = A.Fake<IMailTemplateManager>();
        var parameters = GetSmtpClientParameters();

        var mailSender = new SmtpMailSender(parameters, templateManager, logger);

        // Act
        mailSender.TryConnect();

        // Assert
        A.CallTo(logger)
            .Where(c => IsLogTraceCall(c))
            .MustHaveHappenedANumberOfTimesMatching(x => x == TryConnectMinimumTraceCount);
    }

    [Test]
    public async Task TryConnectAsync_DefaultParameters_ShouldLogExpectedTraces()
    {
        // Arrange
        var logger = A.Fake<ILogger<SmtpMailSender>>();
        var templateManager = A.Fake<IMailTemplateManager>();
        var parameters = GetSmtpClientParameters();

        var mailSender = new SmtpMailSender(parameters, templateManager, logger);

        // Act
        await mailSender.TryConnectAsync();

        // Assert
        A.CallTo(logger)
            .Where(c => IsLogTraceCall(c))
            .MustHaveHappenedANumberOfTimesMatching(x => x == TryConnectMinimumTraceCount);
    }

    [Test]
    public void TryConnect_ProtocolLoggerEnabled_ShouldLogMoreTraces()
    {
        // Arrange
        var logger = A.Fake<ILogger<SmtpMailSender>>();
        var templateManager = A.Fake<IMailTemplateManager>();
        var parameters = GetSmtpClientParameters();
        parameters.ProtocolLogger.Enabled = true;

        var mailSender = new SmtpMailSender(parameters, templateManager, logger);

        // Act
        mailSender.TryConnect();

        // Assert
        A.CallTo(logger)
            .Where(c => IsLogTraceCall(c))
            .MustHaveHappenedANumberOfTimesMatching(x => x > TryConnectMinimumTraceCount);
    }

    [Test]
    public async Task TryConnectAsync_ProtocolLoggerEnabled_ShouldLogMoreTraces()
    {
        // Arrange
        var logger = A.Fake<ILogger<SmtpMailSender>>();
        var templateManager = A.Fake<IMailTemplateManager>();
        var parameters = GetSmtpClientParameters();
        parameters.ProtocolLogger.Enabled = true;

        var mailSender = new SmtpMailSender(parameters, templateManager, logger);

        // Act
        await mailSender.TryConnectAsync();

        // Assert
        A.CallTo(logger)
            .Where(c => IsLogTraceCall(c))
            .MustHaveHappenedANumberOfTimesMatching(x => x > TryConnectMinimumTraceCount);
    }

    [Test]
    public void SendMail_DefaultParameters_ShouldLogExpectedTraces()
    {
        // Arrange
        var logger = A.Fake<ILogger<SmtpMailSender>>();
        var templateManager = A.Fake<IMailTemplateManager>();
        var parameters = GetSmtpClientParameters();

        var mailSender = new SmtpMailSender(parameters, templateManager, logger);
        var mail = CreateTestMail();

        // Act
        mailSender.SendMail(mail);

        // Assert
        A.CallTo(logger)
            .Where(c => IsLogTraceCall(c))
            .MustHaveHappenedANumberOfTimesMatching(x => x == SendMailMinimumTraceCount);
    }

    [Test]
    public async Task SendMailAsync_DefaultParameters_ShouldLogExpectedTraces()
    {
        // Arrange
        var logger = A.Fake<ILogger<SmtpMailSender>>();
        var templateManager = A.Fake<IMailTemplateManager>();
        var parameters = GetSmtpClientParameters();

        var mailSender = new SmtpMailSender(parameters, templateManager, logger);
        var mail = CreateTestMail();

        // Act
        await mailSender.SendMailAsync(mail);

        // Assert
        A.CallTo(logger)
            .Where(c => IsLogTraceCall(c))
            .MustHaveHappenedANumberOfTimesMatching(x => x == SendMailMinimumTraceCount);
    }

    [Test]
    public void SendMail_ProtocolLoggerEnabled_ShouldLogMoreTraces()
    {
        // Arrange
        var logger = A.Fake<ILogger<SmtpMailSender>>();
        var templateManager = A.Fake<IMailTemplateManager>();
        var parameters = GetSmtpClientParameters();
        parameters.ProtocolLogger.Enabled = true;

        var mailSender = new SmtpMailSender(parameters, templateManager, logger);
        var mail = CreateTestMail();

        // Act
        mailSender.SendMail(mail);

        // Assert
        A.CallTo(logger)
            .Where(c => IsLogTraceCall(c))
            .MustHaveHappenedANumberOfTimesMatching(x => x > SendMailMinimumTraceCount);
    }

    [Test]
    public async Task SendMailAsync_ProtocolLoggerEnabled_ShouldLogMoreTraces()
    {
        // Arrange
        var logger = A.Fake<ILogger<SmtpMailSender>>();
        var templateManager = A.Fake<IMailTemplateManager>();
        var parameters = GetSmtpClientParameters();
        parameters.ProtocolLogger.Enabled = true;

        var mailSender = new SmtpMailSender(parameters, templateManager, logger);
        var mail = CreateTestMail();

        // Act
        await mailSender.SendMailAsync(mail);

        // Assert
        A.CallTo(logger)
            .Where(c => IsLogTraceCall(c))
            .MustHaveHappenedANumberOfTimesMatching(x => x > SendMailMinimumTraceCount);
    }

    private static SmtpClientParameters GetSmtpClientParameters()
    {
        var parameters = new SmtpClientParameters
        {
            Host = "localhost",
            Port = 41025
        };

        var hostEnvVar = Environment.GetEnvironmentVariable("SMTP_HOST");
        var portEnvVar = Environment.GetEnvironmentVariable("SMTP_PORT");
        if (!string.IsNullOrWhiteSpace(hostEnvVar) && !string.IsNullOrWhiteSpace(portEnvVar))
        {
            parameters.Host = hostEnvVar;
            parameters.Port = Convert.ToInt32(portEnvVar);
        }

        return parameters;
    }

    private static bool IsLogTraceCall(IFakeObjectCall c)
    {
        return c.Method.Name == nameof(ILogger.Log) && c.GetArgument<LogLevel>(0) == LogLevel.Trace;
    }

    private static MimeMessage CreateTestMail()
    {
        var mail = new MimeMessage();
        mail.From.Add(new MailboxAddress("AA", "a@a.io"));
        mail.To.Add(new MailboxAddress("BB", "b@b.io"));
        mail.Subject = "TEST";
        return mail;
    }
}
