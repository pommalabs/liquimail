﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using FakeItEasy;
using Microsoft.Extensions.Logging.Abstractions;
using PommaLabs.Liquimail.Templating;

namespace PommaLabs.Liquimail.UnitTests;

[TestFixture, Parallelizable]
internal sealed class SmtpMailSenderTests
{
    private const string ValidHost = "smtp.example.com";

    [Test]
    public void ConstructorWithParameters_DisableServerCertificateValidation_ShouldNotThrow()
    {
        // Arrange
        var logger = NullLogger<SmtpMailSender>.Instance;
        var templateManager = A.Fake<IMailTemplateManager>();
        var parameters = new SmtpClientParameters
        {
            Host = ValidHost,
            DisableServerCertificateValidation = true,
        };

        // Act & Assert
        Assert.DoesNotThrow(() => new SmtpMailSender(parameters, templateManager, logger));
    }

    [Test]
    public void ConstructorWithParameters_MinimalValidParameters_ShouldNotThrow()
    {
        // Arrange
        var logger = NullLogger<SmtpMailSender>.Instance;
        var templateManager = A.Fake<IMailTemplateManager>();
        var parameters = new SmtpClientParameters
        {
            Host = ValidHost,
        };

        // Act & Assert
        Assert.DoesNotThrow(() => new SmtpMailSender(parameters, templateManager, logger));
    }
}
