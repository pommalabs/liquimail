﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using DotLiquid;
using DotLiquid.Exceptions;
using MimeKit;
using PommaLabs.Liquimail.Templating;

namespace PommaLabs.Liquimail.UnitTests.Templating;

[TestFixture, Parallelizable(ParallelScope.Fixtures), Category(nameof(Templating))]
public abstract class MailTemplateManagerTestsBase
{
    protected FakeMailSender _fakeMailSender;
    protected IMailTemplateManager _mailTemplateManager;

    #region Setup/Teardown

    [OneTimeSetUp]
    public virtual void OneTimeSetUp()
    {
    }

    [OneTimeTearDown]
    public virtual void OneTimeTearDown()
    {
    }

    [SetUp]
    public virtual void SetUp()
    {
    }

    [TearDown]
    public virtual void TearDown()
    {
        _fakeMailSender = null;
        _mailTemplateManager = null;
    }

    #endregion Setup/Teardown

    [Test]
    public void ShouldLoadAndRenderNestedSimpleTemplate()
    {
        // Arrange
        var simple = _mailTemplateManager.LoadTemplate("nested/simple");

        // Act
        var simpleRender = simple.Render(Hash.FromAnonymousObject(new
        {
            utc_now = DateTimeOffset.UtcNow
        }));

        // Assert
        Assert.That(simpleRender, Contains.Substring("NESTED_HEADER"));
        Assert.That(simpleRender, Contains.Substring("NESTED_SIMPLE"));
        Assert.That(simpleRender, Contains.Substring("NESTED_FOOTER"));
    }

    [Test]
    public void ShouldLoadAndRenderSimpleTemplate()
    {
        // Arrange
        var simple = _mailTemplateManager.LoadTemplate("simple");

        // Act
        var simpleRender = simple.Render(Hash.FromAnonymousObject(new
        {
            utc_now = DateTimeOffset.UtcNow
        }));

        // Assert
        Assert.That(simpleRender, Contains.Substring("ROOT_HEADER"));
        Assert.That(simpleRender, Contains.Substring("ROOT_SIMPLE"));
        Assert.That(simpleRender, Contains.Substring("ROOT_FOOTER"));
    }

    [Test]
    public void ShouldSendCerberusTemplate()
    {
        // Arrange
        var mail = new MimeMessage();
        mail.From.Add(new MailboxAddress("AA", "a@a.io"));
        mail.To.Add(new MailboxAddress("BB", "b@b.io"));
        mail.Subject = "TEST";

        // Act
        _fakeMailSender.SendMailFromTemplate(mail, "cerberus", Hash.FromAnonymousObject(new
        {
            utc_now = DateTimeOffset.UtcNow
        }));

        // Assert
        Assert.That(_fakeMailSender.SentMailMessages, Has.Count.EqualTo(1));

        var sentMail = _fakeMailSender.SentMailMessages[0];
        Assert.That(sentMail.HtmlBody, Contains.Substring("Maecenas sed ante pellentesque"));
        Assert.That(sentMail.TextBody, Contains.Substring("Maecenas sed ante pellentesque"));
    }

    [Test]
    public async Task ShouldSendCerberusTemplateAsync()
    {
        // Arrange
        var mail = new MimeMessage();
        mail.From.Add(new MailboxAddress("AA", "a@a.io"));
        mail.To.Add(new MailboxAddress("BB", "b@b.io"));
        mail.Subject = "TEST";

        // Act
        await _fakeMailSender.SendMailFromTemplateAsync(mail, "cerberus", Hash.FromAnonymousObject(new
        {
            utc_now = DateTimeOffset.UtcNow
        }));

        // Assert
        Assert.That(_fakeMailSender.SentMailMessages, Has.Count.EqualTo(1));

        var sentMail = _fakeMailSender.SentMailMessages[0];
        Assert.That(sentMail.HtmlBody, Contains.Substring("Maecenas sed ante pellentesque"));
        Assert.That(sentMail.TextBody, Contains.Substring("Maecenas sed ante pellentesque"));
    }

    [Test]
    public void ShouldSendNestedSimpleTemplate()
    {
        // Arrange
        var mail = new MimeMessage();
        mail.From.Add(new MailboxAddress("AA", "a@a.io"));
        mail.To.Add(new MailboxAddress("BB", "b@b.io"));
        mail.Subject = "TEST";

        // Act
        _fakeMailSender.SendMailFromTemplate(mail, "nested/simple", Hash.FromAnonymousObject(new
        {
            utc_now = DateTimeOffset.UtcNow
        }));

        // Assert
        Assert.That(_fakeMailSender.SentMailMessages, Has.Count.EqualTo(1));

        var sentMail = _fakeMailSender.SentMailMessages[0];
        Assert.That(sentMail.HtmlBody, Contains.Substring("NESTED_HEADER"));
        Assert.That(sentMail.HtmlBody, Contains.Substring("NESTED_SIMPLE"));
        Assert.That(sentMail.HtmlBody, Contains.Substring("NESTED_FOOTER"));
    }

    [Test]
    public async Task ShouldSendNestedSimpleTemplateAsync()
    {
        // Arrange
        var mail = new MimeMessage();
        mail.From.Add(new MailboxAddress("AA", "a@a.io"));
        mail.To.Add(new MailboxAddress("BB", "b@b.io"));
        mail.Subject = "TEST";

        // Act
        await _fakeMailSender.SendMailFromTemplateAsync(mail, "nested/simple", Hash.FromAnonymousObject(new
        {
            utc_now = DateTimeOffset.UtcNow
        }));

        // Assert
        Assert.That(_fakeMailSender.SentMailMessages, Has.Count.EqualTo(1));

        var sentMail = _fakeMailSender.SentMailMessages[0];
        Assert.That(sentMail.HtmlBody, Contains.Substring("NESTED_HEADER"));
        Assert.That(sentMail.HtmlBody, Contains.Substring("NESTED_SIMPLE"));
        Assert.That(sentMail.HtmlBody, Contains.Substring("NESTED_FOOTER"));
    }

    [Test]
    public void ShouldSendSimpleTemplate()
    {
        // Arrange
        var mail = new MimeMessage();
        mail.From.Add(new MailboxAddress("AA", "a@a.io"));
        mail.To.Add(new MailboxAddress("BB", "b@b.io"));
        mail.Subject = "TEST";

        // Act
        _fakeMailSender.SendMailFromTemplate(mail, "simple", Hash.FromAnonymousObject(new
        {
            utc_now = DateTimeOffset.UtcNow
        }));

        // Assert
        Assert.That(_fakeMailSender.SentMailMessages, Has.Count.EqualTo(1));

        var sentMail = _fakeMailSender.SentMailMessages[0];
        Assert.That(sentMail.HtmlBody, Contains.Substring("ROOT_HEADER"));
        Assert.That(sentMail.HtmlBody, Contains.Substring("ROOT_SIMPLE"));
        Assert.That(sentMail.HtmlBody, Contains.Substring("ROOT_FOOTER"));
    }

    [Test]
    public async Task ShouldSendSimpleTemplateAsync()
    {
        // Arrange
        var mail = new MimeMessage();
        mail.From.Add(new MailboxAddress("AA", "a@a.io"));
        mail.To.Add(new MailboxAddress("BB", "b@b.io"));
        mail.Subject = "TEST";

        // Act
        await _fakeMailSender.SendMailFromTemplateAsync(mail, "simple", Hash.FromAnonymousObject(new
        {
            utc_now = DateTimeOffset.UtcNow
        }));

        // Assert
        Assert.That(_fakeMailSender.SentMailMessages, Has.Count.EqualTo(1));

        var sentMail = _fakeMailSender.SentMailMessages[0];
        Assert.That(sentMail.HtmlBody, Contains.Substring("ROOT_HEADER"));
        Assert.That(sentMail.HtmlBody, Contains.Substring("ROOT_SIMPLE"));
        Assert.That(sentMail.HtmlBody, Contains.Substring("ROOT_FOOTER"));
    }

    [TestCase((string)null)]
    [TestCase("")]
    [TestCase("   ")]
    [TestCase("\t")]
    [TestCase("\r\n")]
    public void ShouldThrowArgumentExceptionWhenRequiredTemplateIsNullEmptyOrBlank(string templateName)
    {
        // Act & Assert
        Assert.Throws<System.ArgumentException>(() => _mailTemplateManager.LoadTemplate(templateName));
    }

    [Test]
    public void ShouldThrowFileSystemExceptionWhenRequiredTemplateDoesNotExist()
    {
        // Arrange
        const string NotExistingTemplateName = "not_existing_123";

        // Act & Assert
        Assert.Throws<FileSystemException>(() => _mailTemplateManager.LoadTemplate(NotExistingTemplateName));
    }

    [Test]
    public void ShouldThrowFileSystemExceptionWhenRequiredTemplateHasAnInvalidName()
    {
        // Arrange
        const string InvalidTemplateName = "@hello!!!";

        // Act & Assert
        Assert.Throws<FileSystemException>(() => _mailTemplateManager.LoadTemplate(InvalidTemplateName));
    }
}
