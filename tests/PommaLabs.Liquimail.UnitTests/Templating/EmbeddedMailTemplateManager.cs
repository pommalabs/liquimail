﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using PommaLabs.Liquimail.Templating;

namespace PommaLabs.Liquimail.UnitTests.Templating;

public sealed class EmbeddedMailTemplateManagerTests : MailTemplateManagerTestsBase
{
    #region Setup/Teardown

    public override void SetUp()
    {
        base.SetUp();
        _mailTemplateManager = new EmbeddedMailTemplateManager(GetType().Assembly, $"{GetType().Namespace}.MailTemplates");
        _fakeMailSender = new FakeMailSender(_mailTemplateManager);
    }

    #endregion Setup/Teardown
}
