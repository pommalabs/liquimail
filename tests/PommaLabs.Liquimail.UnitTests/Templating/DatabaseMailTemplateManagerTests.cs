﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Data;
using System.Data.SQLite;
using PommaLabs.Liquimail.Templating;

namespace PommaLabs.Liquimail.UnitTests.Templating;

public sealed class DatabaseMailTemplateManagerTests : MailTemplateManagerTestsBase
{
    private static string s_connectionString;
    private static SQLiteConnection s_keepAliveConnection;

    #region Setup/Teardown

    public override void OneTimeSetUp()
    {
        base.OneTimeSetUp();

        s_connectionString = new SQLiteConnectionStringBuilder
        {
            FullUri = $"file:{nameof(Liquimail)}?mode=memory&cache=shared",
        }.ConnectionString;

        s_keepAliveConnection = new SQLiteConnection(s_connectionString);
        s_keepAliveConnection.Open();

        using var command = s_keepAliveConnection.CreateCommand();
        command.CommandType = CommandType.Text;

        // Create "MailTemplates" table.
        command.CommandText = @"
            create table MailTemplates (
                Path text primary key,
                Template text not null
            );
        ";
        command.ExecuteNonQuery();

        // Fill new table with test mail templates.
        command.CommandText = @"
            insert into MailTemplates (Path, Template)
            values (@path, @template)
        ";
        var pathParameter = command.Parameters.Add("path", DbType.String);
        var templateParameter = command.Parameters.Add("template", DbType.String);

        foreach (var templateFile in Directory.GetFiles("Templating/MailTemplates"))
        {
            pathParameter.Value = Path.GetFileNameWithoutExtension(templateFile);
            templateParameter.Value = File.ReadAllText(templateFile);
            command.ExecuteNonQuery();
        }
        foreach (var templateFile in Directory.GetFiles("Templating/MailTemplates/nested"))
        {
            pathParameter.Value = "nested/" + Path.GetFileNameWithoutExtension(templateFile);
            templateParameter.Value = File.ReadAllText(templateFile);
            command.ExecuteNonQuery();
        }
    }

    public override void OneTimeTearDown()
    {
        base.OneTimeTearDown();

        s_keepAliveConnection?.Dispose();
    }

    public override void SetUp()
    {
        base.SetUp();

        if (s_keepAliveConnection?.State != ConnectionState.Open)
        {
            throw new InvalidOperationException("Keep-alive connection to shared SQLite in-memory database is not open");
        }

        _mailTemplateManager = new DatabaseMailTemplateManager(SQLiteFactory.Instance, s_connectionString);
        _fakeMailSender = new FakeMailSender(_mailTemplateManager);
    }

    #endregion Setup/Teardown
}
