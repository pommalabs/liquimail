﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using FakeItEasy;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Internal;
using PommaLabs.Liquimail.AspNetCore;

namespace PommaLabs.Liquimail.UnitTests.AspNetCore;

[TestFixture, Parallelizable(ParallelScope.Fixtures), Category(nameof(AspNetCore))]
internal sealed class LiquimailHealthCheckTests
{
    private const string CheckId = nameof(CheckId);

    private static readonly TimeSpan s_expirationDelay = TimeSpan.FromMinutes(1);

    #region Setup/Teardown

    [SetUp]
    public void SetUp()
    {
        LiquimailHealthCheck.ClearCachedResults();
    }

    [TearDown]
    public void TearDown()
    {
        LiquimailHealthCheck.ClearCachedResults();
    }

    #endregion Setup/Teardown

    [Test]
    public async Task ShouldReuseCachedResultWhenHealthCheckFailsAndResultIsStillValid()
    {
        // Arrange
        var mailSender = A.Fake<IMailSender>();
        A.CallTo(() => mailSender.TryConnectAsync(A<CancellationToken>._))
            .Throws(new InvalidOperationException());

        var systemClock = A.Fake<ISystemClock>();
        A.CallTo(() => systemClock.UtcNow).Returns(DateTimeOffset.UtcNow);

        var serviceProvider = A.Fake<IServiceProvider>();
        A.CallTo(() => serviceProvider.GetService(typeof(ISystemClock))).Returns(systemClock);

        var healthCheck = new LiquimailHealthCheck(mailSender, serviceProvider);

        var context = new HealthCheckContext();
        await healthCheck.CheckHealthAsync(context);

        // Act
        await healthCheck.CheckHealthAsync(context);

        // Assert
        A.CallTo(() => mailSender.TryConnectAsync(A<CancellationToken>._))
            .MustHaveHappenedOnceExactly();
    }

    [Test]
    public async Task ShouldReuseCachedResultWhenHealthCheckSucceedsAndResultIsStillValid()
    {
        // Arrange
        var mailSender = A.Fake<IMailSender>();

        var systemClock = A.Fake<ISystemClock>();
        A.CallTo(() => systemClock.UtcNow).Returns(DateTimeOffset.UtcNow);

        var serviceProvider = A.Fake<IServiceProvider>();
        A.CallTo(() => serviceProvider.GetService(typeof(ISystemClock))).Returns(systemClock);

        var healthCheck = new LiquimailHealthCheck(mailSender, serviceProvider);

        var context = new HealthCheckContext();
        var firstResult = await healthCheck.CheckHealthAsync(context);

        // Act
        var secondResult = await healthCheck.CheckHealthAsync(context);

        // Assert
        Assert.That(secondResult.Data[CheckId], Is.EqualTo(firstResult.Data[CheckId]));
    }

    [Test]
    public async Task ShouldNotReuseCachedResultWhenHealthCheckFailsAndResultIsNotValid()
    {
        // Arrange
        var mailSender = A.Fake<IMailSender>();
        A.CallTo(() => mailSender.TryConnectAsync(A<CancellationToken>._))
            .Throws(new InvalidOperationException());

        var systemClock = A.Fake<ISystemClock>();
        A.CallTo(() => systemClock.UtcNow).Returns(DateTimeOffset.UtcNow);

        var serviceProvider = A.Fake<IServiceProvider>();
        A.CallTo(() => serviceProvider.GetService(typeof(ISystemClock))).Returns(systemClock);

        var healthCheck = new LiquimailHealthCheck(mailSender, serviceProvider);

        var context = new HealthCheckContext();
        await healthCheck.CheckHealthAsync(context);

        A.CallTo(() => systemClock.UtcNow).Returns(DateTimeOffset.UtcNow + s_expirationDelay);

        // Act
        await healthCheck.CheckHealthAsync(context);

        // Assert
        A.CallTo(() => mailSender.TryConnectAsync(A<CancellationToken>._))
            .MustHaveHappenedTwiceExactly();
    }

    [Test]
    public async Task ShouldNotReuseCachedResultWhenHealthCheckSucceedsAndResultIsNotValid()
    {
        // Arrange
        var mailSender = A.Fake<IMailSender>();

        var systemClock = A.Fake<ISystemClock>();
        A.CallTo(() => systemClock.UtcNow).Returns(DateTimeOffset.UtcNow);

        var serviceProvider = A.Fake<IServiceProvider>();
        A.CallTo(() => serviceProvider.GetService(typeof(ISystemClock))).Returns(systemClock);

        var healthCheck = new LiquimailHealthCheck(mailSender, serviceProvider);

        var context = new HealthCheckContext();
        var firstResult = await healthCheck.CheckHealthAsync(context);

        A.CallTo(() => systemClock.UtcNow).Returns(DateTimeOffset.UtcNow + s_expirationDelay);

        // Act
        var secondResult = await healthCheck.CheckHealthAsync(context);

        // Assert
        Assert.That(secondResult.Data[CheckId], Is.Not.EqualTo(firstResult.Data[CheckId]));
    }

    [Test]
    public async Task ShouldNotReuseCachedResultAcrossDifferentCaches()
    {
        // Arrange
        var systemClock = A.Fake<ISystemClock>();
        A.CallTo(() => systemClock.UtcNow).Returns(DateTimeOffset.UtcNow);

        var serviceProvider = A.Fake<IServiceProvider>();
        A.CallTo(() => serviceProvider.GetService(typeof(ISystemClock))).Returns(systemClock);

        var firstMailSender = A.Fake<IMailSender>();
        var firstHealthCheck = new LiquimailHealthCheck(firstMailSender, serviceProvider);
        var firstContext = new HealthCheckContext();

        var secondMailSender = A.Fake<IMailSender>();
        var secondHealthCheck = new LiquimailHealthCheck(secondMailSender, serviceProvider);
        var secondContext = new HealthCheckContext();

        // Act
        var firstResult = await firstHealthCheck.CheckHealthAsync(firstContext);
        var secondResult = await secondHealthCheck.CheckHealthAsync(secondContext);

        // Assert
        Assert.That(secondResult.Data[CheckId], Is.Not.EqualTo(firstResult.Data[CheckId]));
    }
}
