﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using PommaLabs.Liquimail.Core;

namespace PommaLabs.Liquimail.UnitTests.Core;

[TestFixture, Parallelizable, Category(nameof(Core))]
internal sealed class ConcurrentLRUCacheTests
{
    [TestCase(2)]
    [TestCase(8)]
    [TestCase(32)]
    public void Add_WhenAboveCapacity_ShouldBeAbleToHandleConcurrentInserts(int uniqueItemCount)
    {
        // Arrange
        const int RepetitionCount = 10;
        var capacity = uniqueItemCount / 2;
        var lruCache = new ConcurrentLRUCache<int, int>(capacity);

        // Act
        Parallel.For(0, uniqueItemCount, i =>
        {
            for (var j = 0; j < RepetitionCount; ++j)
            {
                lruCache.Add(i, j);
            }
        });

        // Assert
        Assert.That(lruCache.Count, Is.LessThanOrEqualTo(capacity));
    }

    [TestCase(1)]
    [TestCase(4)]
    [TestCase(16)]
    public void Add_WhenBelowCapacity_ShouldBeAbleToHandleConcurrentInserts(int uniqueItemCount)
    {
        // Arrange
        const int RepetitionCount = 10;
        var capacity = uniqueItemCount;
        var lruCache = new ConcurrentLRUCache<int, int>(capacity);

        // Act
        Parallel.For(0, uniqueItemCount, i =>
        {
            for (var j = 0; j < RepetitionCount; ++j)
            {
                lruCache.Add(i, j);
                Assert.That(lruCache.TryGetValue(i, out var k), Is.True);
                Assert.That(k, Is.EqualTo(j));
            }
        });

        // Assert
        Assert.That(lruCache.Count, Is.LessThanOrEqualTo(capacity));
    }
}
