﻿using System.Data.SQLite;
using MailKit.Net.Smtp;
using Microsoft.Extensions.DependencyInjection;
using PommaLabs.Liquimail.Templating;

namespace PommaLabs.Liquimail.UnitTests.Core;

[TestFixture, Parallelizable, Category(nameof(Core))]
internal sealed class LiquimailServiceCollectionExtensionsTests
{
    [Test]
    public void AddDatabaseMailTemplateManager_WhenCalled_ShouldRegisterServices()
    {
        // Arrange
        var services = new ServiceCollection();

        // Act
        services.AddDatabaseMailTemplateManager(SQLiteFactory.Instance, "ConnectionString");

        // Assert
        var serviceProvider = services.BuildServiceProvider();
        Assert.That(serviceProvider.GetRequiredService<IMailTemplateManager>(), Is.InstanceOf<DatabaseMailTemplateManager>());
    }

    [Test]
    public void AddEmbeddedMailTemplateManager_WhenCalled_ShouldRegisterServices()
    {
        // Arrange
        var services = new ServiceCollection();

        // Act
        services.AddEmbeddedMailTemplateManager(GetType().Assembly, "RootNamespace");

        // Assert
        var serviceProvider = services.BuildServiceProvider();
        Assert.That(serviceProvider.GetRequiredService<IMailTemplateManager>(), Is.InstanceOf<EmbeddedMailTemplateManager>());
    }

    [Test]
    public void AddLocalMailTemplateManager_WhenCalled_ShouldRegisterServices()
    {
        // Arrange
        var services = new ServiceCollection();

        // Act
        services.AddLocalMailTemplateManager("AbsolutePath");

        // Assert
        var serviceProvider = services.BuildServiceProvider();
        Assert.That(serviceProvider.GetRequiredService<IMailTemplateManager>(), Is.InstanceOf<LocalMailTemplateManager>());
    }

    [Test]
    public void AddFakeMailSender_WhenCalledWithoutMailTemplateManager_ShouldRegisterItselfAndNotSupportedMailTemplateManager()
    {
        // Arrange
        var services = new ServiceCollection();

        // Act
        services.AddFakeMailSender();

        // Assert
        var serviceProvider = services.BuildServiceProvider();
        Assert.That(serviceProvider.GetRequiredService<IMailSender>(), Is.InstanceOf<FakeMailSender>());
        Assert.That(serviceProvider.GetRequiredService<IMailTemplateManager>(), Is.InstanceOf<NotSupportedMailTemplateManager>());
    }

    [Test]
    public void AddFakeMailSender_WhenCalledWithMailTemplateManager_ShouldOnlyRegisterItself()
    {
        // Arrange
        var services = new ServiceCollection();

        // Act
        services.AddFakeMailSender()
            .AddLocalMailTemplateManager("AbsolutePath");

        // Assert
        var serviceProvider = services.BuildServiceProvider();
        Assert.That(serviceProvider.GetRequiredService<IMailSender>(), Is.InstanceOf<FakeMailSender>());
        Assert.That(serviceProvider.GetRequiredService<IMailTemplateManager>(), Is.InstanceOf<LocalMailTemplateManager>());
    }

    [Test]
    public void AddSmtpMailSender_WhenCalledWithParameters_ShouldRegisterServices()
    {
        // Arrange
        var services = new ServiceCollection();
        services.AddLogging();
        var smtpClientParameters = CreateFakeSmtpClientParameters();

        // Act
        services.AddSmtpMailSender(smtpClientParameters);

        // Assert
        var serviceProvider = services.BuildServiceProvider();
        Assert.That(serviceProvider.GetRequiredService<IMailSender>(), Is.InstanceOf<SmtpMailSender>());
    }

    [Test]
    public void AddSmtpMailSender_WhenCalledWithoutMailTemplateManager_ShouldRegisterItselfAndNotSupportedMailTemplateManager()
    {
        // Arrange
        var services = new ServiceCollection();
        services.AddLogging();
        var smtpClientParameters = CreateFakeSmtpClientParameters();

        // Act
        services.AddSmtpMailSender(_ => smtpClientParameters);

        // Assert
        var serviceProvider = services.BuildServiceProvider();
        Assert.That(serviceProvider.GetRequiredService<IMailSender>(), Is.InstanceOf<SmtpMailSender>());
        Assert.That(serviceProvider.GetRequiredService<IMailTemplateManager>(), Is.InstanceOf<NotSupportedMailTemplateManager>());
    }

    [Test]
    public void AddSmtpMailSender_WhenCalledWithMailTemplateManager_ShouldOnlyRegisterItself()
    {
        // Arrange
        var services = new ServiceCollection();
        services.AddLogging();
        var smtpClientParameters = CreateFakeSmtpClientParameters();

        // Act
        services.AddSmtpMailSender(_ => smtpClientParameters)
            .AddLocalMailTemplateManager("AbsolutePath");

        // Assert
        var serviceProvider = services.BuildServiceProvider();
        Assert.That(serviceProvider.GetRequiredService<IMailSender>(), Is.InstanceOf<SmtpMailSender>());
        Assert.That(serviceProvider.GetRequiredService<IMailTemplateManager>(), Is.InstanceOf<LocalMailTemplateManager>());
    }

    [Test]
    public void AddSmtpMailSender_WhenCalledWithClientBuilder_ShouldRegisterServices()
    {
        // Arrange
        var services = new ServiceCollection();
        services.AddLogging();
        var smtpClientParameters = CreateFakeSmtpClientParameters();

        // Act
        services.AddSmtpMailSender(() => new SmtpClient());

        // Assert
        var serviceProvider = services.BuildServiceProvider();
        Assert.That(serviceProvider.GetRequiredService<IMailSender>(), Is.InstanceOf<SmtpMailSender>());
    }

    [Test]
    public void AddSmtpMailSender_WhenCalledWithAsyncClientBuilder_ShouldRegisterServices()
    {
        // Arrange
        var services = new ServiceCollection();
        services.AddLogging();
        var smtpClientParameters = CreateFakeSmtpClientParameters();

        // Act
        services.AddSmtpMailSender(_ => Task.FromResult(new SmtpClient()));

        // Assert
        var serviceProvider = services.BuildServiceProvider();
        Assert.That(serviceProvider.GetRequiredService<IMailSender>(), Is.InstanceOf<SmtpMailSender>());
    }

    private static SmtpClientParameters CreateFakeSmtpClientParameters()
    {
        return new()
        {
            Host = "Host",
            Port = 25,
            UserName = "UserName",
            Password = "Password",
        };
    }
}
