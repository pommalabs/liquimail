# Liquimail

[![License: MIT][project-license-badge]][project-license]
[![Donate][paypal-donations-badge]][paypal-donations]
[![Docs][docfx-docs-badge]][docfx-docs]
[![NuGet version][nuget-version-badge]][nuget-package]
[![NuGet downloads][nuget-downloads-badge]][nuget-package]

[![standard-readme compliant][github-standard-readme-badge]][github-standard-readme]
[![GitLab pipeline status][gitlab-pipeline-status-badge]][gitlab-pipelines]
[![Quality gate][sonar-quality-gate-badge]][sonar-website]
[![Code coverage][sonar-coverage-badge]][sonar-website]
[![Renovate enabled][renovate-badge]][renovate-website]

Liquimail implements glue code to use Liquid templates with MailKit.

**Library is feature complete and no further development is planned on this project,
except for routine maintenance and bug fixes.**

Liquimail templates are processed using [DotLiquid][github-dotliquid] library
and mail messages are sent with [MailKit][github-mailkit].

Liquid templates can be stored in three different ways:

- Local file system,
- Embedded resources,
- SQL database table.

A fake, in-memory mail sender is also provided in order to easily perform unit tests.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
  - [Editing](#editing)
  - [Restoring dependencies](#restoring-dependencies)
  - [Running tests](#running-tests)
- [License](#license)

## Install

NuGet package [PommaLabs.Liquimail][nuget-package] is available for download:

```bash
dotnet add package PommaLabs.Liquimail
```

Another package, [PommLabs.Liquimail.AspNetCore][nuget-package-aspnetcore],
which is focused on ASP.NET Core integrations, is also available for download:

```bash
dotnet add package PommaLabs.Liquimail.AspNetCore
```

## Usage

Liquimail exposes two main interfaces:

- [IMailSender][liquimail-imailsender], which actually sends mail messages.
- [IMailTemplatesManager][liquimail-imailtemplatesmanager], which loads mail templates.

Configuring the first interface is mandatory, while the second one is needed
only when using `SendMail` overloads which use the templating system based on [DotLiquid][github-dotliquid].

## Maintainers

[@pomma89][gitlab-pomma89].

## Contributing

MRs accepted.

Small note: If editing the README, please conform to the [standard-readme][github-standard-readme] specification.

### Editing

[Visual Studio Code][vscode-website], with [Remote Containers extension][vscode-remote-containers],
is the recommended way to work on this project.

A development container has been configured with all required tools.

[Visual Studio Community][vs-website] is also supported
and an updated solution file, `liquimail.sln`, has been provided.

### Restoring dependencies

When opening the development container, dependencies should be automatically restored.

Anyway, dependencies can be restored with following command:

```bash
dotnet restore
```

### Running tests

Tests can be run with following command:

```bash
dotnet test
```

Tests can also be run with following command, which collects coverage information:

```bash
./build.sh --target run-tests
```

## License

MIT © 2018-2025 [PommaLabs Team and Contributors][pommalabs-website]

[docfx-docs]: https://liquimail-docs.pommalabs.xyz/
[docfx-docs-badge]: https://img.shields.io/badge/DocFX-OK-green?style=flat-square
[github-dotliquid]: https://github.com/dotliquid/dotliquid
[github-mailkit]: https://github.com/jstedfast/MailKit
[github-standard-readme]: https://github.com/RichardLitt/standard-readme
[github-standard-readme-badge]: https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square
[gitlab-pipeline-status-badge]: https://gitlab.com/pommalabs/liquimail/badges/main/pipeline.svg?style=flat-square
[gitlab-pipelines]: https://gitlab.com/pommalabs/liquimail/pipelines
[gitlab-pomma89]: https://gitlab.com/pomma89
[liquimail-imailsender]: https://gitlab.com/pommalabs/liquimail/-/blob/main/src/PommaLabs.Liquimail/IMailSender.cs
[liquimail-imailtemplatesmanager]: https://gitlab.com/pommalabs/liquimail/-/blob/main/src/PommaLabs.Liquimail/Templating/IMailTemplateManager.cs
[nuget-downloads-badge]: https://img.shields.io/nuget/dt/PommaLabs.Liquimail?style=flat-square
[nuget-package]: https://www.nuget.org/packages/PommaLabs.Liquimail/
[nuget-package-aspnetcore]: https://www.nuget.org/packages/PommaLabs.Liquimail.AspNetCore/
[nuget-version-badge]: https://img.shields.io/nuget/v/PommaLabs.Liquimail?style=flat-square
[paypal-donations]: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=ELJWKEYS9QGKA
[paypal-donations-badge]: https://img.shields.io/badge/Donate-PayPal-important.svg?style=flat-square
[pommalabs-website]: https://pommalabs.xyz/
[project-license]: https://gitlab.com/pommalabs/liquimail/-/blob/main/LICENSE
[project-license-badge]: https://img.shields.io/badge/License-MIT-yellow.svg?style=flat-square
[renovate-badge]: https://img.shields.io/badge/renovate-enabled-brightgreen.svg?style=flat-square
[renovate-website]: https://renovate.whitesourcesoftware.com/
[sonar-coverage-badge]: https://img.shields.io/sonar/coverage/pommalabs_liquimail?server=https%3A%2F%2Fsonarcloud.io&sonarVersion=8&style=flat-square
[sonar-quality-gate-badge]: https://img.shields.io/sonar/quality_gate/pommalabs_liquimail?server=https%3A%2F%2Fsonarcloud.io&sonarVersion=8&style=flat-square
[sonar-website]: https://sonarcloud.io/dashboard?id=pommalabs_liquimail
[vs-website]: https://visualstudio.microsoft.com/
[vscode-remote-containers]: https://code.visualstudio.com/docs/remote/containers
[vscode-website]: https://code.visualstudio.com/
